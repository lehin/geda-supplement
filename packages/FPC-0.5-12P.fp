
Element["" "" "" "" 53.0000mm 53.0000mm 0.0000 0.0000 0 100 ""]
(
	Pad[4.4300mm -1.8400mm 4.4300mm -1.5400mm 2.7000mm 40.00mil 3.7160mm "" "1" "square"]
	Pad[-4.9300mm -1.8400mm -4.9300mm -1.5400mm 2.7000mm 40.00mil 3.7160mm "" "2" "square"]
	Pad[2.5000mm -4.0000mm 2.5000mm -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "12" "square"]
	Pad[2.0000mm -4.0000mm 2.0000mm -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "11" "square"]
	Pad[1.5000mm -4.0000mm 1.5000mm -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "10" "square"]
	Pad[1.0000mm -4.0000mm 1.0000mm -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "9" "square"]
	Pad[0.5000mm -4.0000mm 0.5000mm -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "8" "square"]
	Pad[0.0000 -4.0000mm 0.0000 -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "7" "square"]
	Pad[-0.5000mm -4.0000mm -0.5000mm -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "6" "square"]
	Pad[-1.0000mm -4.0000mm -1.0000mm -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "5" "square"]
	Pad[-1.5000mm -4.0000mm -1.5000mm -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "4" "square"]
	Pad[-2.0000mm -4.0000mm -2.0000mm -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "3" "square"]
	Pad[-2.5000mm -4.0000mm -2.5000mm -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "2" "square"]
	Pad[-3.0000mm -4.0000mm -3.0000mm -3.1000mm 0.3000mm 20.00mil 0.8080mm "" "1" "square"]
	ElementLine [-6.1000mm 0.0000 5.6000mm 0.0000 10.00mil]
	ElementLine [-6.1000mm 2.0000mm -6.1000mm 0.0000 10.00mil]
	ElementLine [5.6000mm 2.0000mm 5.6000mm 0.0000 10.00mil]
	ElementLine [-6.1000mm 2.0000mm 5.6000mm 2.0000mm 10.00mil]

	)
