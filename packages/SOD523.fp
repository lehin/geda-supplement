
Element["" "" "" "" 50.9500mm 41.8000mm 0.0000 0.0000 0 100 ""]
(
	Pad[0.0000 -0.7000mm 0.0000 -0.6500mm 0.4000mm 20.00mil 0.9080mm "" "2" "square"]
	Pad[0.0000 0.6500mm 0.0000 0.7000mm 0.4000mm 20.00mil 0.9080mm "" "1" "square,edge2"]
	ElementLine [-0.4000mm -0.6000mm 0.4000mm -0.6000mm 0.1500mm]
	ElementLine [0.4000mm -0.6000mm 0.4000mm 0.6000mm 0.1500mm]
	ElementLine [0.4000mm 0.6000mm -0.4000mm 0.6000mm 0.1500mm]
	ElementLine [-0.4000mm 0.6000mm -0.4000mm -0.6000mm 0.1500mm]
	ElementLine [-0.4000mm -0.2500mm 0.4000mm -0.2500mm 0.1500mm]

	)
