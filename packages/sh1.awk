BEGIN {
	base_unit_mm = 0

	help_auto()

	proc_args(P, "n", "n")

	P["n"] = int(P["n"])
	if (P["n"] < 2)
		error("Number of pins must be greater then 2")

	subc_begin(P["n"], "XS1", 0, 0)

   y0 = mm(2.5)
   step = mm(1)

   pad_l = mm(4.2-2.65)
	proto = subc_proto_create_pad_rect(mm(0.6), pad_l)
	proto_fix = subc_proto_create_pad_rect(mm(1.2), mm(1.8))

	for(n = 1; n <= P["n"]; n++) {
		x = mm(n-1)
		subc_pstk(proto, x - (P["n"]-1)/2*step, y0 - pad_l/2)
	}

   fix_offs_x = mm((P["n"]-1)/2 + 0.7 + 1.2/2)
   fix_offs_y = y0 - mm(4.2 - 1.8/2)

   subc_pstk(proto_fix, fix_offs_x, fix_offs_y, 0, 0)
   subc_pstk(proto_fix, -fix_offs_x, fix_offs_y, 0, 0)
   
   # silk
   size_x = mm(P["n"] + 2)
   size_y = mm(2.9)
   
   subc_line("top-silk", -size_x/2 + mm(1.2), -mm(1.385), size_x/2 - mm(1.2), -mm(1.385))
   
   y1 = mm(1.415)
   y2 = y1 - mm(0.9)

   subc_line("top-silk", -size_x/2, y1, -size_x/2, y2)
   subc_line("top-silk", size_x/2, y1, size_x/2, y2)
   subc_line("top-silk", -size_x/2, y1, -size_x/2 + mm(0.7), y1)
   subc_line("top-silk", size_x/2, y1, size_x/2 - mm(0.7), y1)

	subc_end()
}
