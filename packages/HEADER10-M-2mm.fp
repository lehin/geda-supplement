
Element["" "" "" "" 25.0000mm 63.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-4.0000mm -1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "2" "edge2"]
	Pin[-4.0000mm 1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "1" "square,edge2"]
	Pin[-2.0000mm 1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "3" "edge2"]
	Pin[-2.0000mm -1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "4" "edge2"]
	Pin[0.0000 -1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "6" "edge2"]
	Pin[0.0000 1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "5" "edge2"]
	Pin[2.0000mm 1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "7" "edge2"]
	Pin[2.0000mm -1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "8" "edge2"]
	Pin[4.0000mm -1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "10" "edge2"]
	Pin[4.0000mm 1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "9" "edge2"]
	ElementLine [-5.0000mm -2.0000mm 5.0000mm -2.0000mm 15.00mil]
	ElementLine [5.0000mm -2.0000mm 5.0000mm 2.0000mm 15.00mil]
	ElementLine [5.0000mm 2.0000mm -5.0000mm 2.0000mm 15.00mil]
	ElementLine [-5.0000mm 2.0000mm -5.0000mm -2.0000mm 15.00mil]

	)
