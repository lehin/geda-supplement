
Element["" "" "" "" 125.00mil 100.00mil 0.0000 0.0000 0 100 ""]
(
	Pad[-1.2000mm -0.8000mm -1.0000mm -0.8000mm 1.2000mm 20.00mil 1.7080mm "" "4" "square"]
	Pad[-1.2000mm 0.8000mm -1.0000mm 0.8000mm 1.2000mm 20.00mil 1.7080mm "" "1" "square"]
	Pad[1.0000mm 0.8000mm 1.2000mm 0.8000mm 1.2000mm 20.00mil 1.7080mm "" "2" "square,edge2"]
	Pad[1.0000mm -0.8000mm 1.2000mm -0.8000mm 1.2000mm 20.00mil 1.7080mm "" "3" "square,edge2"]
	ElementLine [-80.00mil -65.00mil 80.00mil -65.00mil 7.00mil]
	ElementLine [80.00mil -65.00mil 80.00mil 65.00mil 7.00mil]
	ElementLine [80.00mil 65.00mil -70.00mil 65.00mil 7.00mil]
	ElementLine [-80.00mil 55.00mil -80.00mil -65.00mil 7.00mil]
	ElementLine [-80.00mil 55.00mil -70.00mil 65.00mil 7.00mil]

	)
