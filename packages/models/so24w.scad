use <packages.scad>

module part_so24w()
{
   rotate ([0, 0, 90])
       SOIC (24, wide = true) ;
}