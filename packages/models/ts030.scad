module part_ts030()
{
   color ([0.3, 0.3, 0.3])
   {
      hull()
      {
         translate ([-11.8/2, -4.1])
            cube ([11.8, 4.1+1.75, 2.4]) ;
            
         translate ([0, 0.35])
            cylinder (r = 8.9 - 4.45, h = 2.4) ;
      }

      intersection()
      {
         translate ([0, 0.35, 2.2 - 1.4])
         {
            R = 11.75-4.45 ;
            difference()
            {
               cylinder (r = R, h = 1.4) ;
               translate ([0, 0, -1])
                  cylinder (r = R-1.2, h = 1.4 + 2) ;
            }
         }

         linear_extrude (10)
            polygon ([ [0, 0], [100 * sin(30), 100 * cos(30)], [-100 * sin(30), 100 * cos(30)] ]) ;
      }

      for (x = [-2.5, 2.5])
         translate ([x, 0, -0.5])
            cylinder (d = 1, h = 0.5) ;
   }

   color ([0.85, 0.85, 0.85])
   {
      for (x = [-2.5 : 2.5 : 2.5])
         translate ([-x - 1.5/2, -4.2, -0.001])
            cube ([1.5, 1.2, 0.1]) ;
      
      for (n = [-1,1])
         translate ([n * 4.5-0.75, 0.25, -0.001])
            cube ([1.5, 1.5, 0.1]) ;
   }
}