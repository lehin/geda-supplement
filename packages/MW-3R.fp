
Element["" "" "" "" 29.0000mm 45.5000mm 0.0000 0.0000 0 100 ""]
(
	Pin[2.0000mm 0.0000 1.5000mm 20.00mil 1.6524mm 0.8000mm "" "3" "edge2"]
	Pin[0.0000 0.0000 1.5000mm 20.00mil 1.6524mm 0.8000mm "" "2" "edge2"]
	Pin[-2.0000mm 0.0000 1.5000mm 20.00mil 1.6524mm 0.8000mm "" "1" "square,edge2"]
	ElementLine [-4.0000mm -6.2500mm -4.0000mm -0.5000mm 10.00mil]
	ElementLine [4.0000mm -6.2500mm -4.0000mm -6.2500mm 10.00mil]
	ElementLine [4.0000mm -0.5000mm 4.0000mm -6.2500mm 10.00mil]
	ElementLine [-4.0000mm -0.5000mm 4.0000mm -0.5000mm 10.00mil]

	)
