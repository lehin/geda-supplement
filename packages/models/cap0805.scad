use <chip-c.scad>

module part_cap0603()
{
   chip_c (L=2.05, T=0.35, W=1.25, H=1.25) ;
}
