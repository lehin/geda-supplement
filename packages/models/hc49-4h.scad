module part_hc49_4h()
{
   $fn = 30 ;
   
   color ([0.90, 0.90, 0.90])
      translate ([4.88/2, 0, 0])
      {
         hull()
         {
            translate ([(-10.77+4.34)/2, 0, 0])
               cylinder (d = 4.34, h = 0.5) ;

            translate ([(10.77-4.34)/2, 0, 0])
               cylinder (d = 4.34, h = 0.5) ;
         }

         hull()
         {
            translate ([(-10.26+3.68)/2, 0, 0])
               cylinder (d = 3.65, h = 3.5) ;

            translate ([(10.26-3.68)/2, 0, 0])
               cylinder (d = 3.68, h = 3.5) ;
         }

         translate ([0, 0, -2.5])
         {
            translate ([-4.88/2, 0, 0])
               cylinder (d = 0.43, h = 2.5) ;

            translate ([4.88/2, 0, 0])
               cylinder (d = 0.43, h = 2.5) ;
         }
      }
}