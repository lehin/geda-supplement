use <packages.scad>

module part_so8()
{
   rotate ([0, 0, 90])
       SOIC (8) ;
}
