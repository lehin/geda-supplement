
Element["" "" "" "" 21.2000mm 10.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-11.2000mm 0.0000 3.0000mm 30.00mil 3.1524mm 1.4000mm "" "1" "edge2"]
	Pin[11.2000mm 0.0000 3.0000mm 30.00mil 3.1524mm 1.4000mm "" "2" "edge2"]
	ElementLine [-11.2000mm -5.3000mm -11.2000mm 5.3000mm 10.00mil]
	ElementLine [-11.2000mm 5.3000mm 11.2000mm 5.3000mm 10.00mil]
	ElementLine [11.2000mm 5.3000mm 11.2000mm -5.3000mm 10.00mil]
	ElementLine [11.2000mm -5.3000mm -11.2000mm -5.3000mm 10.00mil]

	)
