
Element["" "" "" "" 21.0000mm 61.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-3.0000mm -1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "1" "square,edge2"]
	Pin[-3.0000mm 1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "2" "edge2"]
	Pin[-1.0000mm 1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "4" "edge2"]
	Pin[-1.0000mm -1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "3" "edge2"]
	Pin[1.0000mm -1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "5" "edge2"]
	Pin[1.0000mm 1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "6" "edge2"]
	Pin[3.0000mm 1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "8" "edge2"]
	Pin[3.0000mm -1.0000mm 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "7" "edge2"]
	ElementLine [-4.0000mm -2.0000mm 4.0000mm -2.0000mm 15.00mil]
	ElementLine [4.0000mm 2.0000mm -4.0000mm 2.0000mm 15.00mil]
	ElementLine [-4.0000mm 2.0000mm -4.0000mm -2.0000mm 15.00mil]
	ElementLine [4.0000mm -2.0000mm 4.0000mm 2.0000mm 15.00mil]

	)
