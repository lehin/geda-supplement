use <packages.scad>

module part_so20w()
{
   rotate ([0, 0, 90])
       SOIC (20, wide = true) ;
}
