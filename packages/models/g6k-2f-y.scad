module part_g6k_2f_y()
{
   module leads()
   {
      X = [ 0, 3.2, 5.4, 7.6 ] ;
      Y_offs = (7.8 - 6.5) / 2 ;
      
      for (x = X)
         translate ([1.2 + x - 0.5/2, 0])
         {
            translate ([0, -Y_offs])
               cube ([0.5, Y_offs, 0.3]) ;

            translate ([0, -0.1])
               cube ([0.5, 0.3, 1.6]) ;
         }
   }
   
   translate ([-10/2, -6.5/2])
   {
      color ([0.9, 0.9, 0.7])
         cube ([10, 6.5, 5.2]) ;
      
      color ([0.95, 0.95, 0.95])
      {
         leads() ;
         
         translate ([0, 6.5])
            mirror ([0, 1, 0])
               leads() ;
      }
   }
}
