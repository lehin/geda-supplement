use <pcb.scad>

module part_cap10x21()
{
   translate ([200/1000*25.4/2, 0])
      cap10x21() ;
}
