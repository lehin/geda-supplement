
Element["" "" "" "" 20.0000mm 18.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-3.1500mm 5.1500mm 2.0000mm 20.00mil 2.1524mm 1.0000mm "" "3" ""]
	Pin[3.1500mm 5.1500mm 2.0000mm 20.00mil 2.1524mm 1.0000mm "" "1" "square"]
	Pin[3.1500mm -5.1500mm 2.0000mm 20.00mil 2.1524mm 1.0000mm "" "6" ""]
	Pin[-3.1500mm -5.1500mm 2.0000mm 20.00mil 2.1524mm 1.0000mm "" "4" ""]
	Pin[0.4000mm 5.1500mm 1.5000mm 20.00mil 1.6524mm 0.8000mm "" "2" ""]
	Pin[0.4000mm -5.1500mm 1.5000mm 20.00mil 1.6524mm 0.8000mm "" "5" ""]
	ElementLine [-5.0000mm -5.0000mm 5.0000mm -5.0000mm 10.00mil]
	ElementLine [5.0000mm -5.0000mm 5.0000mm 5.0000mm 10.00mil]
	ElementLine [5.0000mm 5.0000mm -5.0000mm 5.0000mm 10.00mil]
	ElementLine [-5.0000mm 5.0000mm -5.0000mm -5.0000mm 10.00mil]

	)
