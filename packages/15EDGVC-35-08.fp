
Element["" "" "" "" 19.0000mm 20.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[413.39mil 0.0000 90.00mil 20.00mil 96.00mil 30.00mil "" "4" "edge2"]
	Pin[275.59mil 0.0000 90.00mil 20.00mil 96.00mil 30.00mil "" "3" "edge2"]
	Pin[0.0000 0.0000 90.00mil 20.00mil 96.00mil 30.00mil "" "1" "square,edge2"]
	Pin[137.80mil 0.0000 90.00mil 20.00mil 96.00mil 30.00mil "" "2" "edge2"]
	Pin[964.57mil 0.0000 90.00mil 20.00mil 96.00mil 30.00mil "" "8" "edge2"]
	Pin[21.0000mm 0.0000 90.00mil 20.00mil 96.00mil 30.00mil "" "7" "edge2"]
	Pin[14.0000mm 0.0000 90.00mil 20.00mil 96.00mil 30.00mil "" "5" "edge2"]
	Pin[688.98mil 0.0000 90.00mil 20.00mil 96.00mil 30.00mil "" "6" "edge2"]
	ElementLine [-2.1500mm -3.5000mm 26.6500mm -3.5000mm 10.00mil]
	ElementLine [26.6500mm -3.5000mm 26.6500mm 3.5000mm 10.00mil]
	ElementLine [26.6500mm 3.5000mm -2.1500mm 3.5000mm 10.00mil]
	ElementLine [-2.1500mm 3.5000mm -2.1500mm -3.5000mm 10.00mil]

	)
