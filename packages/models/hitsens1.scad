module part_hitsens1()
{
   sz = [18.2, 5.8, 5.6] ;

   color ([0.8, 0.8, 0.8])
      translate ([0, -0.1, -2.5])
      {
         translate ([-13/2 - 0.8/2, 0])
            cube ([0.8, 0.2, 2.5]) ;

         translate ([13/2 - 0.8/2, 0])
            cube ([0.8, 0.2, 2.5]) ;
      }

   color ([0.95, 0.95, 0.95])
      translate ([-sz[0]/2, -sz[1]+1])
         cube (sz) ;
}
