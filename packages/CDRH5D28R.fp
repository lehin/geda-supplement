
Element["" "" "" "" 4.5000mm 4.5000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-2.8001mm -21.65mil -2.8001mm 21.65mil 39.37mil 20.00mil 1.5080mm "" "1" "square"]
	Pad[2.8001mm -21.65mil 2.8001mm 21.65mil 39.37mil 20.00mil 1.5080mm "" "2" "square"]
	ElementLine [-3.5000mm -3.4000mm -3.5000mm 3.4000mm 7.00mil]
	ElementLine [-3.5000mm 3.4000mm 3.5000mm 3.4000mm 7.00mil]
	ElementLine [3.5000mm 3.4000mm 3.5000mm -3.4000mm 7.00mil]
	ElementLine [3.5000mm -3.4000mm -3.5000mm -3.4000mm 7.00mil]

	)
