
Element["" "" "" "" 400.00mil 1320.00mil 0.0000 0.0000 0 100 ""]
(
	Pin[0.0000 -0.5000mm 0.6016mm 0.5000mm 0.7540mm 0.5000mm "" "" ""]
	Pin[0.0000 0.4500mm 0.6016mm 0.5000mm 0.7540mm 0.5000mm "" "" ""]
	Pad[0.0000 -0.4500mm 0.0000 0.4500mm 1.5000mm 0.7000mm 1.7250mm "" "" "square"]
	Pad[1.2500mm -1.0000mm 1.4500mm -1.0000mm 0.4000mm 0.7000mm 0.6250mm "" "6" "square"]
	Pad[1.2500mm 1.0000mm 1.4500mm 1.0000mm 0.4000mm 0.7000mm 0.6250mm "" "4" "square"]
	Pad[-1.4500mm -1.0000mm -1.2500mm -1.0000mm 0.4000mm 0.7000mm 0.6250mm "" "1" "square"]
	Pad[-1.4500mm 0.0000 -1.2500mm 0.0000 0.4000mm 0.7000mm 0.6250mm "" "2" "square"]
	Pad[-1.4500mm 1.0000mm -1.2500mm 1.0000mm 0.4000mm 0.7000mm 0.6250mm "" "3" "square"]
	Pad[1.2500mm 0.0000 1.4500mm 0.0000 0.4000mm 0.7000mm 0.6250mm "" "5" "square"]
	ElementLine [-1.4000mm -1.5000mm 1.8000mm -1.5000mm 0.1250mm]
	ElementLine [1.8000mm -1.5000mm 1.8000mm 1.5000mm 0.1250mm]
	ElementLine [1.8000mm 1.5000mm -1.8000mm 1.5000mm 0.1250mm]
	ElementLine [-1.8000mm 1.5000mm -1.8000mm -1.1000mm 0.1250mm]
	ElementLine [-1.8000mm -1.1000mm -1.4000mm -1.5000mm 0.1250mm]

	)
