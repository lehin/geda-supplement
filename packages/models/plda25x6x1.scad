use <pcb.scad>
module part_plda25x6x1 (flip = false)
{
   mirror ([flip ? 1 : 0, 0, 0])
      rotate ([0, 0, -90])
         plda1 (6) ;
}
