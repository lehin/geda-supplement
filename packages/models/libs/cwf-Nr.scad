module cwf_Nr (cnt)
{
   d = 0.64 ;
   H = 5.75 ;
   
   h = H - 2.35 ;
   
   module lead()
   {
      $fn = 30 ;
      
      r = 0.2 ;
      
      translate ([-d/2, -d/2, -3.4])
         cube ([d, d, 3.4 + h - r - d/2]) ;
      
      translate ([-d/2, r + d/2, h - d/2])
         cube ([d, 6 - r - d/2, d]) ;
      
      translate ([0, r + d/2, h - r - d/2])
         rotate ([90, 0, -90])
            rotate_extrude (angle=90)
               translate ([r, -d/2])
                  square ([d, d]) ;
   }

   for (n = [0 : cnt-1])
      translate ([-(cnt-1)/2*2.5 + n*2.5, 0])
         color ([1, 1, 0.6])
            lead() ;
   
   color ("white")
      translate ([-(cnt*2.5 + 2.4)/2, 0.3, 0])
      {
         difference()
         {
            cube ([cnt*2.5 + 2.4, 7, H]) ;
            
            translate ([0.5, 0.5, 0.5])
               cube ([cnt*2.5 + 2.4 - 1, 7, H - 1]) ;

            translate ([(cnt*2.5 + 2.4) / 2 - 3/2, 2, 0.5])
               cube ([3, 7, H + 1]) ;
         }
      }
}

cwf_Nr (2) ;