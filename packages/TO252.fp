
Element["" "" "" "" 10.0000mm 10.0000mm 0.0000 0.0000 0 100 ""]
(
	Pad[100.00mil 5.4000mm 100.00mil 7.1000mm 0.8000mm 20.00mil 1.3080mm "" "5" "square,edge2"]
	Pad[50.00mil 5.4000mm 50.00mil 7.1000mm 0.8000mm 20.00mil 1.3080mm "" "4" "square,edge2"]
	Pad[-100.00mil 5.4000mm -100.00mil 7.1000mm 0.8000mm 20.00mil 1.3080mm "" "1" "square,edge2"]
	Pad[-50.00mil 5.4000mm -50.00mil 7.1000mm 0.8000mm 20.00mil 1.3080mm "" "2" "square,edge2"]
	Pad[0.0000 5.4000mm 0.0000 7.1000mm 0.8000mm 20.00mil 1.3080mm "" "3" "square,edge2"]
	Pad[0.0000 0.0000 0.0100mm 0.0000 6.0000mm 40.00mil 7.0160mm "" "GND" "square"]
	ElementLine [-3.2500mm -3.2500mm 3.2500mm -3.2500mm 0.1500mm]
	ElementLine [3.2500mm -3.2500mm 3.2500mm 6.2500mm 0.1500mm]
	ElementLine [3.2500mm 6.2500mm -3.2500mm 6.2500mm 0.1500mm]
	ElementLine [-3.2500mm 6.2500mm -3.2500mm -3.2500mm 0.1500mm]

	)
