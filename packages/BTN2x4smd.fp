
Element["" "" "" "" 8.0000mm 9.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-0.8500mm 0.0000 0.8016mm 0.02mil 0.9540mm 0.7000mm "" "GND" "edge2"]
	Pin[0.8500mm 0.0000 0.8016mm 0.02mil 0.9540mm 0.7000mm "" "GND" "edge2"]
	Pad[1.7000mm 1.5000mm 1.7000mm 1.6000mm 1.2000mm 0.8000mm 2.0000mm "" "2" "square,edge2"]
	Pad[-1.7000mm 1.5000mm -1.7000mm 1.6000mm 1.2000mm 0.8000mm 2.0000mm "" "1" "square,edge2"]
	Pad[2.3000mm -0.1500mm 2.3000mm 0.1500mm 0.9000mm 20.00mil 1.4080mm "" "GND" "square"]
	Pad[-2.3000mm -0.1500mm -2.3000mm 0.1500mm 0.9000mm 20.00mil 1.4080mm "" "GND" "square"]
	ElementLine [-2.3000mm 1.7000mm 2.3000mm 1.7000mm 0.1000mm]
	ElementLine [2.3000mm 1.7000mm 2.3000mm -0.6000mm 0.1000mm]
	ElementLine [2.3000mm -0.6000mm -2.3000mm -0.6000mm 0.1000mm]
	ElementLine [-2.3000mm 1.7000mm -2.3000mm -0.6000mm 0.1000mm]

	)
