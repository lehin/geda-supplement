use <rcube.scad>

delta=1e-3 ;

module terminal50 (cnt)
{
      for (n = [ 0 : cnt - 1])
         translate ([n * 5, 0, 0])
         {
            color ([0.4, 0.4, 0.8])
               translate ([-2.5, -3.75])
                  difference()
                  {
                     hull()
                     {
                        cube ([5, 7.5, 6.9]) ;
                     
                        translate ([0, 1.25, 9])
                           cube ([5, 5, 1]) ;
                     }
                     
                     translate ([2.5, 3.75, 8])
                        cylinder (d = 3.5, h = 2 + delta) ;
                  }

            color([0.9,0.9,0.9])
            {
               translate ([0, 0, 8])
                  cylinder (d = 3.5, h = 1) ;
               
               translate ([0, 0, -2.5])
                  cylinder (d = 0.9, h = 2.5) ;
            }
         }
}

module terminal35 (cnt)
{
      for (n = [ 0 : cnt - 1])
         translate ([n * 3.5 - 3.5/2, -3.5, 0])
         {
            color ([0.75, 0.9, 0.4])
               difference()
               {
                  hull()
                  {
                     cube ([3.5, 7, 5]) ;
                  
                     translate ([0, 1.5, 7.7])
                        cube ([3.5, 4, 1]) ;
                  }
                  
                  translate ([1.75, 3.5, 6.7])
                     cylinder (d = 2.5, h = 2 + delta) ;
               }

            color([0.9,0.9,0.9])
            {
               translate ([1.75, 3.5, 6.7])
                  cylinder (d = 2.5, h = 1) ;

               translate ([1.75, 3.5, -2.5])
                  cylinder (d = 0.8, h = 2.5) ;
            }
         }
}

module pld (cnt, rows, step = 2.54)
{
   for (r = [ 0 : rows - 1])
      for (n = [ 0 : cnt - 1])
         translate([n * step, r * step, 0])
         {
            color ("black")
               translate ([-step / 2, -step / 2, 0])
                  cube ([step, step, step * 0.8]) ;

            translate ([-step/4 / 2, -step/4 / 2, -2.5])
               color ([1, 1, 0.6])
                  cube ([step/4, step/4, 8 + 2.5]) ;
         }
}

module plda1 (cnt)
{
   $fn = 30 ;
   
   A = 6.2 ;
   B = 3.2 ;
   C = 10.8 ;
   d = 0.64 ;

   module lead()
   {
      r = 0.25 ;
      l = C - r - d/2 ;

      color ([1, 1, 0.6])
      {
         translate ([-d/2, r + d/2, 1.25 - d/2])
            cube ([d, l, d]) ;

         translate ([-d/2, -d/2, -B])
            cube ([d, d, B + 1.25 - r - d/2]) ;

         translate ([0, r + d/2, 1.25 - r - d/2])
            rotate ([90, 0, -90])
               rotate_extrude (angle=90)
                  translate ([r, -d/2])
                     square ([d, d]) ;
      }
   }

   for (n = [ 0 : cnt - 1])
      translate ([2.54 * n, 0, 0])
         lead () ;
   
   color ("black")
      translate ([-2.54/2, C-A-2.5-d/2, 0])
         cube ([2.54*cnt, 2.5, 2.5]) ;
}

module led (offset = 50)
{
   color ("white")
      translate ([0,0,offset])
      {
         H = 177 + 31 - 118/2 ;
         cylinder (d = 118, h = H) ;
         cylinder (d = 154, h = 31) ;
         translate ([0, 0, H - delta])
            sphere (d = 118) ;
      }
      
   color ([0.9, 0.9, 0.9])
   {
      translate ([-50-10, -10, 0])
         cube ([20, 20, offset]) ;

      translate ([50-10, -10, 0])
         cube ([20, 20, offset]) ;
   }
}

module photoresistor (offset = 2.54)
{
   color ([0.9, 0.7, 0.7])
      translate ([0, 0, offset])
      {
         intersection()
         {
            D = 5.1 ;
            H = 1.8 ;
            C = 4.3 ;
            
            cylinder (d = D, h = H) ;
            
            translate ([-D/2, -C/2, 0])
               cube ([D, C, H]) ;
         }
      }

   P = 3.4 ;

   color ([0.9, 0.9, 0.9])
   {
      translate ([-P/2, 0, 0])
         cylinder (d = 0.5, h = offset) ;

      translate ([P/2, 0, 0])
         cylinder (d = 0.5, h = offset) ;
   }
}

module fresnel()
{
   B = 23 ;
   Bh = 3 ;
   H = 15 ;
   
   color ([0.9, 0.9, 0.9])
   {
      translate ([-B/2, -B/2, 0])
         cube ([B, B, Bh]) ;

      hull()
      {
         cylinder (d = B, h = Bh) ;

         translate ([0, 0, H - B/2])
            intersection()
            {
               sphere (d = B, $fn = 15) ;

               translate ([-B/2, -B/2, 0])
                  cube ([B, B, B/2]) ;
            }
      }
   }
}

module esp07()
{
   size = [22, 16, 1] ;
   shield = [15, 12, 2.5] ;
   
   translate ([-size[0] / 2, -size[1] / 2, 0])
   {
      color ([0, 0.2, 0.5])
         cube (size) ;
      
      color ([0.9, 0.9, 0.9])
         translate ([size[0] - shield[0] - 1.2, (size[1] - shield[1]) / 2, size[2]])
            cube (shield) ;
   }
}

module bh10r()
{
   cnt = 5 ;
   step = 2.54 ;
   h = [6,6] - [0, step] ;
   
   for (r = [0 : 1])
      for (n = [ 0 : cnt - 1])
         translate([n * step, r * step, 0])
         {
            translate ([-step/4 / 2, -step/4 / 2, 0])
               color ([1, 1, 0.6])
                  cube ([step/4, step/4, h[r]]) ;
         }

   for (r = [0 : 1])
      for (n = [ 0 : cnt - 1])
         translate([n * step, r * step, h[r] - step/4/2])
         {
            rotate ([-90, 0, 0])
               translate ([-step/4 / 2, -step/4 / 2, 0])
                  color ([1, 1, 0.6])
                     cube ([step/4, step/4, 13 - r * step]) ;
         }
         
   case = [20.35, 8.85, 9] ;
   
   color ([0.3, 0.3, 0.3])      
      translate ([-(case[0] - step*4) / 2, 4.5, 0])
         difference()
         {
            cube (case) ;
            
            th = 0.9 ;
            translate ([th, th, th])
               cube (case - [th*2, 0, th*2]) ;
         }
}

module cdrh5d28r()
{
   sz = [6.2, 6.3, 3.0] ;
   
   color ([0.3, 0.3, 0.3])
      render()
         intersection()
         {
            translate ([-sz[0]/2, -sz[1]/2, 0])
               cube (sz) ;
         
            sz2 = [sz[0] * 2, 7.2, sz[2]] ;
            rotate ([0,0,-45])
               translate ([-sz2[0]/2, -sz2[1]/2, 0])
                  cube (sz2) ;
         }
}

module cdrh3d16()
{
   sz = [3.8, 3.8, 1.8] ;
   
   color ([0.3, 0.3, 0.3])
      render()
         intersection()
         {
            translate ([-sz[0]/2, -sz[1]/2, 0])
               cube (sz) ;
         
            sz2 = [sz[0] * 2, 4.4, sz[2]] ;
            rotate ([0,0,-45])
               translate ([-sz2[0]/2, -sz2[1]/2, 0])
                  cube (sz2) ;
         }
}

module cdrh127()
{
   sz = [12.8, 12.8, 8] ;
   
   color ([0.3, 0.3, 0.3])
      render()
         translate ([-sz[0]/2, -sz[1]/2, 0])
            rcube (sz, rdim = 1) ;
}

module cd54()
{
   color ([0.3, 0.3, 0.3])
      intersection()
      {
         union()
         {
            difference()
            {
               cylinder (d = 6.1, h = 0.8) ;
               translate ([0, -6.1/2, -delta])
                  cylinder (d = 0.6, h = 0.8 + 2*delta) ;

               translate ([0, 6.1/2, -delta])
                  cylinder (d = 0.6, h = 0.8 + 2*delta) ;
            }
            
            translate ([0, 0, 4.8 - 1.2])
               cylinder (d = 6.1, h = 1.2) ;
         }
         
         translate ([-5.5/2, -6.1/2, 0])
            cube ([5.5, 6.1, 4.8]) ;
      }
   
   color ([0.9, 0.8, 0.5])
      translate ([0, 0, 0.8])
         cylinder (d = 5.5, h = 2.8) ;
}

module cd43()
{
   color ([0.3, 0.3, 0.3])
      intersection()
      {
         union()
         {
            difference()
            {
               cylinder (d = 4.5, h = 0.8) ;
               translate ([0, -4.5/2, -delta])
                  cylinder (d = 0.6, h = 0.8 + 2*delta) ;

               translate ([0, 4.5/2, -delta])
                  cylinder (d = 0.6, h = 0.8 + 2*delta) ;
            }
            
            translate ([0, 0, 3.2 - 1.2])
               cylinder (d = 4.5, h = 1.2) ;
         }
         
         translate ([-4/2, -4.5/2, 0])
            cube ([4, 4.5, 3.2]) ;
      }
   
   color ([0.9, 0.8, 0.5])
      translate ([0, 0, 0.8])
         cylinder (d = 3.8, h = 3.2 - 1.6) ;
}

module usb_5s_b()
{
   sz = [7.80, 5.00 + 0.65, 2.80] ;
   base = [sz[0] / 2, 2.15 + 0.65, (sz[2] - 2.35) / 2] ;

   color ([0.7, 0.7, 0.7])
      translate (-base)
         cube (sz) ;
}

module msk12c02 (pos = false)
{
   sz = [6.7, 2.7, 1.4] ;
   base = [sz[0] / 2, sz[1]/2, 0] ;

   translate (-base)
      color ([0.7, 0.7, 0.7])
         cube (sz) ;
   
   handle = [1.3, 1.5, 1.1] ;
   
   translate ([(pos ? -1.5 : 1.5) / 2 - handle[0] / 2, sz[1]/2, 0.1])
      color ([1, 1, 1])
         cube (handle) ;
}

module fuse_fh102()
{
   translate ([-22/2, -10.5/2])
   {
      color ([0.9, 0.9, 0.7])
         cube ([22, 10.5, 5.5]) ;
      
      color ([0.8, 0.8, 0.8])
      {
         translate ([0, 1.5, 5.5])
            cube ([5, 7.5, 14.4-5.5]) ;

         translate ([22 - 5, 1.5, 5.5])
            cube ([5, 7.5, 14.4-5.5]) ;
      }

      color ([1,1,1])
         translate ([1, 10.5 / 2, 14 - 6.5/2])
            rotate ([0, 90, 0])
               cylinder (d = 6.5, h = 20) ;
   }

   color ([1, 1, 0.6])
      translate ([0, 0, -2.5])
      {
         translate ([-0.3/2 - 22.4/2, -1.4/2])
            cube ([0.3, 1.4, 5.5]) ;

         translate ([-0.3/2 + 22.4/2, -1.4/2])
            cube ([0.3, 1.4, 5.5]) ;
      }
}

module varistor_s05kx()
{
   color ([0.9, 0.7, 0.3])
      translate ([5.08/2, 2.5, 3.5])
         rotate ([90, 0, 0])
            cylinder (d = 5, h = 4) ;

   color ([0.9, 0.9, 0.9])
      translate ([0, 0, -2.5])
      {
         cylinder (d = 0.5, h = 3.5 + 2.5) ;

         translate ([5.08, 0])
            cylinder (d = 0.5, h = 3.5 + 2.5) ;
      }
   
}

module hlk_pm01()
{
   sz = [34, 20.2, 15] ;
   
   color ([0.5, 0.5, 0.9])
      translate ([-(sz[0] - 29.4)/2, -(sz[1]/2 + 2.5)])
         cube (sz) ;

   leads = [ [0, 0], [0, -5], [29.4, 10.4/2], [29.4, -10.4/2 - 5] ] ;
   
   color ([0.9, 0.9, 0.9])
      translate ([0, 0, -2.5])
         for (p = leads)
            translate (p)
               cylinder (d = 0.7, h = 2.5) ;
}

module hcm1206x()
{
   color ([0.3, 0.3, 0.3])
      difference()
      {
         cylinder (d = 12, h = 9.5) ;
         translate ([0, 0, 7])
            cylinder (d = 2, h = 3) ;
      }

   color ("white")
      translate ([0, 0, 7])
         cylinder (d = 2, h = 1) ;

   color ([0.9, 0.9, 0.9])
   {
      translate ([-7.6/2, 0, -2.5])
         cylinder (d = 0.6, h = 2.5) ;

      translate ([7.6/2, 0, -2.5])
         cylinder (d = 0.6, h = 2.5) ;
   }
}

module mq135()
{
   color ([0.9, 0.9, 0.9])
      cylinder (d = 19.5, h = 5.3) ;

   color ([0.7, 0.7, 0.7])
      hull()
      {
         translate ([0, 0, 5.3-delta])
            cylinder (d = 16, h = delta) ;

         translate ([0, 0, 14.6])
            cylinder (d = 13, h = delta) ;
      }
      
   Rl = 9.5 / 2 ;
   Ll = 6.5 ;
   dl = 1 ;
      
   color ("white")
      for (n = [0:7])
      {
         a = 360 / 8 * n ;
         
         if (n != 2 && n != 6)
            translate ([Rl * cos (a), Rl * sin (a), -Ll])
               cylinder (d = dl, h = Ll) ;
      }
}

module mq3()
{
   color ([0.9, 0.6, 0])
      cylinder (d = 16.5, h = 9) ;

   Rl = 9.5 / 2 ;
   Ll = 6.5 ;
   dl = 1 ;
      
   color ("white")
      for (n = [0:7])
      {
         a = 360 / 8 * n ;
         
         if (n != 2 && n != 6)
            translate ([Rl * cos (a), Rl * sin (a), -Ll])
               cylinder (d = dl, h = Ll) ;
      }
}

module wl102_transmitter()
{
      translate ([0, -5.2, 0])
      {
         translate ([-(17 - 2.54*3)/2, 0, 0])
         {
            color ([0, 0.7, 0])
               cube ([17, 1.2, 13]) ;
            
            color ([0.9, 0.9, 0.9])
               translate ([17 + 2.5, 1.2 + 4.5/2, 11.5 + 4.5/2])
                  rotate ([0, 90, 0])
                     cylinder (d = 4.5, h = 14) ;
         }
         
         color ([0.2, 0.2, 0.2])
            translate ([-2.54/2, 1.2])
               cube ([2.54 * 4, 2.5, 2.5]) ;
         
         color ([0.9, 0.9, 0.9])
            for (i = [0:3])
               translate ([i * 2.54, 2.5 + 1.2, 2.5/2])
               {
                  translate ([-0.3, 0, -0.3])
                     cube ([0.6, 5.5 - 2.5 - 1.2, 0.6]) ;

                  translate ([-0.3, 5.2 - 2.5 - 1.2 - 0.3, -2.5/2 - 2.5])
                     cube ([0.6, 0.6, 2.5/2+0.3 + 2.5]) ;
               }
      }
}

module hps16()
{
   color ([0.5, 0.5, 0.5])
      translate ([-8, -8])
         cube ([16, 16, 3]) ;
}

module cdrh104r()
{
   color ([0.4, 0.4, 0.4])
      translate ([-10.5/2, -10.5/2])
         cube ([10.5, 10.5, 4]) ;
}

module oled091()
{
      color ([0.6, 0.6, 0.6])
         cube ([26.6, 11.5, 1.2]) ;
 
      color ([0.3, 0.3, 0.3])
         translate ([2.1, 2.1])
            cube ([22.384, 5.584, 1.2 + delta]) ;
}

module btn2x4r()
{
   color ([1, 1, 0.7])
   {
      translate ([-4.4/2, -1.1])
         cube ([4.4, 2, 1.8]) ;

      translate ([1.7/2, 0, -0.4])
         cylinder (d = 0.6, h = 0.4) ;

      translate ([-1.7/2, 0, -0.4])
         cylinder (d = 0.6, h = 0.4) ;
   }

   translate ([-1.9/2, 0.9])
      color ([0, 0, 0])
         cube ([1.9, 1.2, 1]) ;
}

module xsm127x2 (cnt)
{
   $fn = 30 ;

   d = 0.4 ;
   r = 0.2 ;
   s = 1.27 ;
   L = 5.5 ;
   l = (L - s) / 2 - r - d/2 ;
   B = 4 ;
   h1 = 2.92 ;
   W = 3.43 ;
   H = 2.51 ;
   
   module lead()
   {
      color ([1, 1, 0.6])
      {
         translate ([-d/2, l, r + d])
            rotate ([0, 90, 0])
               rotate_extrude (angle = 90)
                  translate ([r, 0, 0])
                     square ([d, d]) ;
         
         translate ([-d/2, 0, 0])
            cube ([d, l, d]) ;
         
         translate ([-d/2, (L - s)/2 - d/2, r + d])
            cube ([d, d, B + h1 - r - d]) ;
      }
   }

   for (n = [0 : cnt-1]) 
      translate ([-(cnt-1)/2 * s + s * n, -L/2])
      {
         lead() ;
         
         translate ([0, L, 0])
            mirror ([0,1,0])
               lead() ;
      }
      
   translate ([-cnt * s/2, -W/2, h1-H])
      color ("black")
         cube ([cnt * s, W, H]) ;
}

module xsm127x2r (num = 3)
{
   thick = 0.5 ;
   B = 4 ;
   H = (3 - 1.27) / 2 ;
   
      translate ([-1.27*num/2, (5.21 - 0.81 + 2.29)/2])
      {
         color ([0.3, 0.3, 0.3])
            cube ([num * 1.27, 2.51, H*2 + 1.27]) ;
         
         color ([1, 0.95, 0])
            for (col = [0:num-1])
               translate ([col * 1.27, 0, 0])
               {
                  a1 = -2.29 + 0.51 + H - thick ;
                  translate ([1.27/2 - thick/2, a1, H - thick/2])
                     cube ([thick, B - a1 + 2.51, thick]) ;

                  a2 = -5.21 + 0.81 + H + 1.27 - thick ;
                  translate ([1.27/2 - thick/2, a2, H + 1.27 - thick/2])
                     cube ([thick, B - a2 + 2.51, thick]) ;

                  translate ([1.27/2 - thick/2, -2.29])
                     cube ([thick, 0.51, thick]) ;

                  translate ([1.27/2 - thick/2, -5.21])
                     cube ([thick, 0.81, thick]) ;
                     
                  translate ([1.27/2 - thick/2, -2.29+0.51])
                     translate ([0, 0, 0])
                        rotate ([45, 0, 0])
                           cube ([thick, (H - thick/2) / sin(45) + thick/2 * cos(45), thick]) ;

                  translate ([1.27/2 - thick/2, -5.21+0.81])
                     rotate ([45, 0, 0])
                        translate ([0, 0,0])
                           cube ([thick, (H + 1.27 - thick/2) / sin(45) + thick/2 * cos(45), thick]) ;
               }
      }
}

module rr3_433()
{
   translate ([-50, 10, 50])
      color ([0, 0.7, 0])
         cube ([1500, 30, 500]) ;
   
   pins = [1, 2, 3, 7, 10, 11, 12, 13, 14, 15] ;
   
   color ([0.8, 0.8, 0.8])
      for (p = pins)
         translate ([(p - 1) * 100 - 15, 0, 0])
            cube ([30, 10, 100]) ;
}

module resistor1w (z = 0)
{
   H = 11 ;
   D = 4.5 ;
   d = 0.8 ;
   L = 500/1000*25.4 ;
   
   translate ([-L + (L - H)/2, 0, z + D/2])
      rotate ([0, 90, 0])
         color ([1, 0.82, 0])
            cylinder (d = D, h = H) ;
   
   color ([0.8, 0.8, 0.8])
   {
      translate ([-L, 0, z + D/2])
         rotate ([0, 90, 0])
         {
            cylinder (d = d, h = (L - H) / 2) ;
            
            translate ([0, 0, (L + H)/2])
               cylinder (d = d, h = (L - H) / 2) ;
         }
         
      translate ([-L, 0, 0])
      {
         cylinder (d = d, h = z + D/2) ;
         
         translate ([0, 0, z + D/2])
            sphere (d = d) ;
      }

      translate ([0, 0, 0])
      {
         cylinder (d = d, h = z + D/2) ;

         translate ([0, 0, z + D/2])
            sphere (d = d) ;
      }
   }
}

module cap10x10()
{
   d = 10 ;
   l = 10 ;
   B = 10.3 ;
   C = 10.3 ;
   A = 3.2 ;
   H = 1.1 ;
   E = 4.5 ;
   
   color ([0.8, 0.8, 0.8])
      cylinder (d = d, h = l) ;
   
   color ([0.3, 0.3, 0.3])
      linear_extrude (height = 0.3)
      polygon 
         (
            [
               [-C/2, -B/2], [-C/2, B/2 - 1], [-C/2 + 1, B/2],
               [C/2 - 1, B/2], [C/2, B/2 - 1], [C/2, -B/2]
            ]
         ) ;

   color ("white")
   {
      translate ([-H/2, -E/2-A])
         cube ([H, A, 0.2]) ;

      translate ([-H/2, E/2])
         cube ([H, A, 0.2]) ;
   }
}

module cap8x11_5()
{
   D = 8 ;
   l = 11.5 ;
   K = 3.5 ;
   d = 0.6 ;
   
   color ([0.2, 0.2, 0.3])
      cylinder (d = D, h = l) ;
   
   color ([0.8, 0.8, 0.8])
   {
      translate ([-K/2, 0, -2.5])
         cylinder (d = d, h = 2.5) ;

      translate ([+K/2, 0, -2.5])
         cylinder (d = d, h = 2.5) ;
   }
}

module cap10x21()
{
   D = 10 ;
   l = 21 ;
   K = 200/1000*25.4 ;
   d = 0.8 ;
   
   color ([0.2, 0.2, 0.3])
      cylinder (d = D, h = l) ;
   
   color ([0.8, 0.8, 0.8])
   {
      translate ([-K/2, 0, -2.5])
         cylinder (d = d, h = 2.5) ;

      translate ([+K/2, 0, -2.5])
         cylinder (d = d, h = 2.5) ;
   }
}

module bmp280()
{
   color ([0.5, 0.45, 0.4])
      translate ([-2.5/2, -2/2, 0])
         cube ([2.5, 2, 0.13]) ;
   
   color ([0.9, 0.9, 0.6])
      translate ([-2.25/2, -1.75/2, 0])
         cube ([2.25, 1.75, 0.95]) ;
}

module osc3225()
{
   color ([0.75, 0.75, 0.75])
   {
      difference()
      {
         translate ([-3.2/2, -2.5/2])
            cube ([3.2, 2.5, 0.5]) ;
         
         pts = [[0,0], [3.2, 0], [3.2, 2.5], [0, 2.5]] ;
         for (p = pts)
            translate (p - [3.2/2, 2.5/2])
               cylinder (d = 0.3, h = 1, $fn=20) ;
      }
   }
   
   color ([1, 1, 0.8])
      translate ([-2.8/2, -2.1/2, 0.5])
         cube ([2.8, 2.1, 0.2]) ;
}

module eds803()
{
   delta = 1e-3 ;
   
   W = 33 ;
   pitch = 2.54 ;
   leadW = 0.55 ;
   leadT = 0.35 ;
   leadH1 = 4 ;
   leadH2 = 6.35 ;
   mainH = leadH2 - leadH1 ;
   mainW = 30.48 ;
   mainW2 = 22.86 ;
   mainL = 50.8 ;
   mainT = 2 - leadT*2 ;
   mainT2 = 2.7 ;
   vaL = 45.72 ;
   vaW = 16.51 ;
   
   module lead()
   {
      translate ([-leadW/2, -leadT/2, -leadH1])
         cube ([leadW, leadT, leadH1]) ;

      translate ([-leadW/2, -leadT/2, 0])
         hull()
         {
            cube ([leadW, leadT, delta]) ;
            
            translate ([0, (W - mainW)/2 - leadT/2, mainH - leadT])
               cube ([leadW, leadT, delta]) ;
         }

      translate ([-1.6/2, (W - mainW)/2 - leadT, mainH - leadT])
      {
         cube ([1.6, leadT, mainT + 2*leadT]) ;
         cube ([1.6, 2, leadT]) ;
         translate ([0, 0, mainT + leadT])
            cube ([1.6, 2, leadT]) ;
      }
   }
   
   translate ([-pitch*9, -W/2])
   {
      color ([0.95, 0.95, 0.95])
         for (n = [0:19])
            translate ([n * pitch, 0])
            {
               lead() ;

               translate ([0, W, 0])
                  mirror ([0,1,0])
                     lead () ;
            }
      
      %render()
      {
         translate ([-pitch/2, (W-mainW)/2, mainH])
            cube ([mainL, mainW, mainT]) ;

         translate ([-pitch/2, (W-mainW2)/2, mainH])
            cube ([mainL, mainW2, mainT2]) ;

         translate ([-pitch/2-1, (W-10)/2, mainH])
            cube ([1+delta, 10, mainT2]) ;
      }

      translate ([-pitch/2, (W-mainW2)/2, mainH-0.05])
         color ("white")
            cube ([mainL, mainW2, 0.06]) ;

      translate ([(pitch*19 - vaL) / 2, (W-vaW)/2, mainH + mainT2])
         %cube ([vaL, vaW, delta]) ;
   }
}

module cap16x25r()
{
   module lead()
   {
      translate ([0, 1, 8 - 1])
         rotate ([180, -90, 0])
            rotate_extrude (angle = 90)
               translate ([1, 0])
                  circle (d = 0.8) ;
      
      translate ([0, 0, -2.5])
         cylinder (d = 0.8, h = 8 + 2.5 - 1) ;
      
      translate ([0, 1, 8])
         rotate ([-90, 0, 0])
            cylinder (d = 0.8, h = 1.27 - 1) ;
   }
   
   translate ([0, 1.27, 8])
      rotate ([-90, 0, 0])
         color ([0.4, 0.4, 0.4])
            cylinder (d = 16, h = 25) ;
   
   color ([0.8, 0.8, 0.8])
   {
      translate ([7.5 / 2, 0])
         lead() ;

      translate ([-7.5 / 2, 0,])
         lead() ;
   }
}

module cap10x16r()
{
   module lead()
   {
      translate ([0, 1, 5 - 1])
         rotate ([180, -90, 0])
            rotate_extrude (angle = 90)
               translate ([1, 0])
                  circle (d = 0.6) ;
      
      translate ([0, 0, -2.5])
         cylinder (d = 0.6, h = 5 + 2.5 - 1) ;
      
      translate ([0, 1, 5])
         rotate ([-90, 0, 0])
            cylinder (d = 0.6, h = 1.27 - 1) ;
   }
   
   translate ([0, 1.27, 5])
      rotate ([-90, 0, 0])
         color ([0.4, 0.4, 0.4])
            cylinder (d = 10, h = 16) ;
   
   color ([0.8, 0.8, 0.8])
   {
      translate ([5 / 2, 0])
         lead() ;

      translate ([-5 / 2, 0,])
         lead() ;
   }
}

module cap8x11r()
{
   module lead()
   {
      translate ([0, 1, 4 - 1])
         rotate ([180, -90, 0])
            rotate_extrude (angle = 90)
               translate ([1, 0])
                  circle (d = 0.6) ;
      
      translate ([0, 0, -2.5])
         cylinder (d = 0.6, h = 4 + 2.5 - 1) ;
      
      translate ([0, 1, 4])
         rotate ([-90, 0, 0])
            cylinder (d = 0.6, h = 1.27 - 1) ;
   }
   
   translate ([0, 1.27, 4])
      rotate ([-90, 0, 0])
         color ([0.4, 0.4, 0.4])
            cylinder (d = 8, h = 11.5) ;
   
   color ([0.8, 0.8, 0.8])
   {
      translate ([3.5 / 2, 0])
         lead() ;

      translate ([-3.5 / 2, 0,])
         lead() ;
   }
}

module st025()
{
   translate ([-3.5, 0, 0])
   {
      color ([0.3, 0.3, 0.3])
         difference()
         {
            union()
            {
               translate ([0, 0, 2.5])
                  rotate ([0, -90, 0])
                     cylinder (d = 5, h = 2.5) ;
                     
               translate ([0, -6.5/2, 0])
                  cube ([11.5, 6.5, 5]) ;

               translate ([7, -5/2, -1])
                  cylinder (d = 0.9, h = 1) ;

               translate ([7, 5/2, -1])
                  cylinder (d = 0.9, h = 1) ;
            }

            translate ([-3, 0, 2.5])
               rotate ([0, 90, 0])
                  cylinder (d = 3.5, h = 13) ;
         }

      color ([0.9, 0.9, 0.9])
      {
         translate ([3.5 - 1/2, -6/2 - 0.1, -2.5])
            cube ([1, 0.2, 2.5]) ;

         translate ([3.5 - 1/2, 6/2 - 0.1, -2.5])
            cube ([1, 0.2, 2.5]) ;

         translate ([11 - 1/2, -4/2 - 0.1, -2.5])
            cube ([1, 0.2, 2.5]) ;

         translate ([11 - 1/2, 4/2 - 0.1, -2.5])
            cube ([1, 0.2, 2.5]) ;
      }
   }
}

module mw_Nm (num)
{
   B = (num - 1) * 2 + 3.9 ;
   b1 = (num - 3) * 2 + 1 ;
   th = 0.6 ;
   
   translate ([-B/2, -(4.8-1.45)])
      color ("white")
         difference()
         {
            cube ([B, 4.8, 6]) ;
            translate ([th, th, th])
               cube ([B - 2*th, 4.8 - 2*th, 6]) ;

            translate ([(B - b1)/2, th, th + 1.5])
               cube ([b1, 4.8, 6]) ;
         }
   
   color ([1, 0.95, 0.7])
      for (n = [0 : num-1])
         translate ([n*2  - (num - 1) - 0.25, -0.25, -3.4])
            cube ([0.5, 0.5, 3.4 + (6-1.5)]) ;
}

module g73y()
{
   module lead()
      color ([0.75, 0.75, 0.75])
         translate ([-4, -0.3, 0.1])
         {
            translate ([0, 0, -0.1])
               cube ([0.5, 0.6, 0.2]) ;

            translate ([0.5, 0, 0])
            {
               rotate ([0, -45, 0])
                  translate ([0, 0, -0.1])
                     cube ([0.5, 0.6, 0.2]) ;

               rotate ([-90, 0, 0])
                  cylinder (d = 0.2, h = 0.6) ;

               translate ([0.5*cos(45), 0, 0.5*sin(45)])
               {
                  rotate ([-90, 0, 0])
                     cylinder (d = 0.2, h = 0.6) ;

                  translate ([0, 0, -0.1])
                     cube ([0.5, 0.6, 0.2]) ;
               }
            }
         } ;
   
   translate ([-3, -3.5/2, 0])
   {
      color ("black")
         cube ([6, 3.5, 0.8]) ;
      
      color ([0.9, 0.9, 0.9])
      {
         translate ([0, 0, 0.8])
               cube ([6, 3.5, 0.55]) ;
      
         translate ([0.75, 0.25, 1.35])
            hull()
            {
               cube ([4.5, 3, delta]) ;
               translate ([0.5, 0.5, 0.5])
                  cube ([3.5, 2, delta]) ;
            }
       }
       
       color ([0.3, 0.3, 0.3])
         translate ([(6-2.65)/2, (3.5-1.3)/2, 1.6])
            cube ([2.65, 1.3, 0.9]) ;
    }
    
    lead() ;
    
    mirror ([1, 0, 0])
      lead() ;
}

module msop8()
{
   SZ = [3.1, 4.5, 1.05] ;
   
   module lead()
      color ([0.8, 0.8, 0.8])
         translate ([-0.15, 0])
         {
            translate ([0, -1.05])
               cube ([0.3, 0.75, 0.15]) ;
            
            translate ([0, -0.3-0.15/2, 0.15/2])
               cube ([0.3, 0.15, 1.05/2]) ;

            translate ([0, -0.3, 1.05/2])
               cube ([0.3, 0.5, 0.15]) ;
          
            translate ([0, -0.3, 0.15/2])
               rotate ([0, 90, 0])
                  cylinder (d = 0.15, h = 0.3) ;

            translate ([0, -0.3, 1.05/2+0.15/2])
               rotate ([0, 90, 0])
                  cylinder (d = 0.15, h = 0.3) ;
         }
         
   module leads()
      for (n = [0 : 3])
         translate ([-0.65*3/2 + n*0.65, -SZ[1]/2])
            lead() ;
   
   translate ([-SZ[0]/2, -SZ[1]/2, 0.15])
   {
      color ([0.2, 0.2, 0.2])
         hull()
         {
            translate ([0.1, 0.1, 0])
               cube (SZ - [0.2, 0.2, 0]) ;
            
            translate ([0, 0, SZ[2]/2])
               cube ([SZ[0], SZ[1], delta]) ;
         }
   }     
  
   leads() ;
   
   mirror ([0, 1, 0])
      leads() ;
}

module vsop8()
{
   SZ = [2.1, 2.4, 0.9] ;
   LS = [0.25, 0, 0.13] ;
   HOFFS = 0.1 ;
   
   module lead()
      color ([0.8, 0.8, 0.8])
         translate ([-0.13, 0])
         {
            translate ([0, -0.4])
               cube ([LS[0], 0.2, LS[2]]) ;
            
            translate ([0, -0.2-LS[2]/2, LS[2]/2])
               cube ([LS[0], LS[2], SZ[2]/2 - LS[2]/2 + HOFFS]) ;

            translate ([0, -0.2, SZ[2]/2 - LS[2]/2 + HOFFS])
               cube ([LS[0], 0.3, LS[2]]) ;
          
            translate ([0, -0.2, LS[2]/2])
               rotate ([0, 90, 0])
                  cylinder (d = LS[2], h = LS[0]) ;

            translate ([0, -0.2, SZ[2]/2 + HOFFS])
               rotate ([0, 90, 0])
                  cylinder (d = LS[2], h = LS[0]) ;
         }
         
   module leads()
      for (n = [0 : 3])
         translate ([-0.5*3/2 + n*0.5, -SZ[1]/2])
            lead() ;
   
   translate ([-SZ[0]/2, -SZ[1]/2, HOFFS])
   {
      color ([0.2, 0.2, 0.2])
         hull()
         {
            translate ([0.05, 0.05, 0])
               cube (SZ - [0.1, 0.1, 0]) ;
            
            translate ([0, 0, SZ[2]/2])
               cube ([SZ[0], SZ[1], delta]) ;
         }
   }     
  
   leads() ;
   
   mirror ([0, 1, 0])
      leads() ;
}

module sot363()
{
   SZ = [2.2, 1.35, 1] ;

   module lead()
      color ([0.8, 0.8, 0.8])
         translate ([-0.15, 0])
         {
            translate ([0, -0.4])
               cube ([0.3, 0.3, 0.15]) ;
            
            translate ([0, -0.1-0.15/2, 0.15/2])
               cube ([0.3, 0.15, 0.9 - 0.15]) ;

            translate ([0, -0.1, 0.9 - 0.15])
               cube ([0.3, 0.3, 0.15]) ;
          
            translate ([0, -0.1, 0.15/2])
               rotate ([0, 90, 0])
                  cylinder (d = 0.15, h = 0.3) ;

            translate ([0, -0.1, 0.9 - 0.15/2])
               rotate ([0, 90, 0])
                  cylinder (d = 0.15, h = 0.3) ;
         }
         
   module leads()
      for (n = [0 : 2])
         translate ([-0.65*2/2 + n*0.65, -SZ[1]/2])
            lead() ;

   translate ([-SZ[0]/2, -SZ[1]/2, 0.1])
   {
      color ([0.2, 0.2, 0.2])
         hull()
         {
            translate ([0.05, 0.05, 0])
               cube (SZ - [0.1, 0.1, 0]) ;
            
            translate ([0, 0, SZ[2] - 0.3])
               cube ([SZ[0], SZ[1], delta]) ;
         }
   }

   leads() ;
   
   mirror ([0, 1, 0])
      leads() ;
}

module to252()
{
   SZ = [6.7, 5.5, 2.3] ;

   module lead()
   {
      W = 0.64 ;
      T = 0.45 ;
      O = 2.5 ;
      L = 1.2 ;
      H = 1.2 ;
      color ([0.8, 0.8, 0.8])
         translate ([-W/2, 0])
         {
            translate ([0, -O])
               cube ([W, L - T/2, T]) ;
            
            translate ([0, -O+L - T, T/2])
               cube ([W, T, H]) ;

            translate ([0, -O+L-T/2, H])
               cube ([W, O-L+T/2, T]) ;
          
            translate ([0, -O+L-T/2, T/2])
               rotate ([0, 90, 0])
                  cylinder (d = T, h = W) ;

            translate ([0, -O+L-T/2, H + T/2])
               rotate ([0, 90, 0])
                  cylinder (d = T, h = W) ;
         }
   }
         
   module leads()
      for (n = [-2 : 2])
         translate ([SZ[0]/2 + n*1.27, 0])
            lead() ;

   translate ([-SZ[0]/2, -5/2 - 2])
   {
      color ([0.2, 0.2, 0.2])
         cube (SZ) ;

      leads() ;
   }
   
   translate ([-5/2, -5/2])
      color ([0.8, 0.8, 0.8])
         cube ([5, 5, 0.5]) ;
}

module to263_5()
{
   SZ = [10.668, 9.169, 4.597] ;

   module lead()
   {
      W = (0.914+0.660)/2 ;
      T = (0.584+0.305)/2 ;
      O = 15.875-9.169-1.676 ;
      L = 2.797 ;
      H = 3.48 - T ;
      color ([0.8, 0.8, 0.8])
         translate ([-W/2, 0])
         {
            translate ([0, -O])
               cube ([W, L - T/2, T]) ;
            
            translate ([0, -O+L - T, T/2])
               cube ([W, T, H]) ;

            translate ([0, -O+L-T/2, H])
               cube ([W, O-L+T/2, T]) ;
          
            translate ([0, -O+L-T/2, T/2])
               rotate ([0, 90, 0])
                  cylinder (d = T, h = W) ;

            translate ([0, -O+L-T/2, H + T/2])
               rotate ([0, 90, 0])
                  cylinder (d = T, h = W) ;
         }
   }
         
   module leads()
      for (n = [-2 : 2])
         translate ([SZ[0]/2 + n*(1.829+1.575)/2, 0])
            lead() ;

   translate ([-SZ[0]/2, 7.747/2 - 1.7 - SZ.y])
   {
      color ([0.2, 0.2, 0.2])
         cube (SZ) ;

      leads() ;
   }
   
   translate ([-6.8/2, -7.747/2])
      color ([0.8, 0.8, 0.8])
      {
         cube ([6.8, 7.747, 1.4]) ;
         
         translate ([-(10.6-6.8)/2, 7.747 - 1.7])
            cube ([10.6, 1.7, 1.4]) ;
      }
}

module ee14()
{
   module ints()
      translate ([-5 - delta, -6.5 - delta, 1])
         cube ([10 + 2*delta, 13 + 2*delta, 9 - 3.5]) ;

   
   color ([0.6, 0.6, 0.6])
      difference()
      {
         translate ([-7, -2.5])
            cube ([14, 5, 11]) ;
         
         ints() ;
      }

   color ("white")
      difference()
      {
         translate ([-5, -6.5])
            cube ([10, 13, 9]) ;
         
         ints() ;
      }
      
   color ("red")
      translate ([0, 0, 1])
         cylinder (d = 10, h = 9-3.5) ;

   PTS = 
   [
      [-5.5/2, -10.1/2], [5.5/2, -10.1/2],
      [-5.5/2, 10.1/2], [5.5/2, 10.1/2]
   ] ;
      
   color ([0.75, 0.75, 0.75])
      for (p = PTS)
         translate ([p[0], p[1], -2.5])
            cylinder (d = 0.7, h = 2.5) ;
}

module dip8()
{
   SZ = [9.9, 6.8, 3.8] ;
   offs = 0.7 ;
   N = 8 ;
   S = 2.54 ;

   lead0 = [ -(N/2-1)*S/2, -7.62/2 ] ;

   module lead()
   {
      color ([0.8, 0.8, 0.8])
      {
         translate ([-1.27/2, -0.25/2, 0])
            cube ([1.27, 0.25, SZ[2] / 2 - 0.25]) ;
         
         translate ([-1.27/2, 0.25, SZ[2] / 2 - 0.25 /2])
            cube ([1.27, 1, 0.25]) ;
         
         translate ([-1.27/2, 0.25, SZ[2] / 2 - 0.25])
            rotate ([0, -90, 180])
               rotate_extrude (angle = 90)
                  translate ([0.25/2, 0])
                     square ([0.25, 1.27]) ;

         hull()
         {
            translate ([-1.27/2, -0.25/2, 0])
               cube ([1.27, 0.25, delta]) ;

            translate ([-0.5/2, -0.25/2, -offs])
               cube ([0.5, 0.25, delta]) ;
         }

            translate ([-0.5/2, -0.25/2, -offs - 2.2])
               cube ([0.5, 0.25, 2.2]) ;
      }
   }

   module leads()
   {
      for (i = [0 : N/2 - 1])
         translate ([i * S + lead0[0], lead0[1], offs])
            lead() ;
   }      
   
   module box()
   {
      color ([0.3, 0.3, 0.3])
         translate ([-SZ[0]/2, -SZ[1]/2, offs])
            difference()
            {
               hull()
               {
                  translate ([0.2, 0.2])
                     cube ([SZ[0] - 0.4, SZ[1] - 0.4, SZ[2]]) ;
                  
                  translate ([0, 0, SZ[2]/2])
                     cube ([SZ[0], SZ[1], delta]) ;
               }
               
               translate ([0, SZ[1]/2, SZ[2] - 0.5])
                  cylinder (d = 3, h = SZ[2]) ;
            }
   }
   
   translate ([-lead0[0], -lead0[1]]) 
   {
      box() ;
      leads() ;
      mirror ([0, 1, 0]) leads() ;
   }
}

module k73_17_0047_400()
{
   r = 2 ;
   sz = [13.5, 6, 10] ;
   offs = 1.5 ;
   d = 0.6 ;
   p = 10 ;
   
   module horz()
   {
      translate ([r, r, r])
         sphere (r = r) ;

      translate ([sz[0]-r, r, r])
         sphere (r = r) ;

      translate ([r, sz[1]-r, r])
         sphere (r = r) ;

      translate ([sz[0]-r, sz[1]-r, r])
         sphere (r = r) ;
   }
   
   module place_leads()
   {
      translate ([-p/2, 0, 0]) children() ;
      translate ([p/2, 0, 0]) children() ;
   }
   
   translate ([p/2, 0, 0])
   {
      color ([0.8, 0.5, 0.3])
      {
         translate ([-sz[0]/2, -sz[1]/2, offs])
            render()
               hull()
               {
                  horz() ;
                  translate ([0, 0, sz[2] - r])
                     horz() ;
               }
               
         place_leads()
            hull()
            {
               cylinder (d = d, h = delta) ;
               
               translate ([0, 0, offs + r])
                  cylinder (d = d + offs*1.5, h = delta) ;
            }
      }
      
      color ([0.8, 0.8, 0.8])
         place_leads()
            translate ([0, 0, -2])
               cylinder (d = d, h = 2 + offs) ;
   }
}

module k73_17_01_400()
{
   r = 2 ;
   sz = [18, 7.5, 12] ;
   offs = 2 ;
   d = 0.6 ;
   p = 15 ;
   
   module horz()
   {
      translate ([r, r, r])
         sphere (r = r) ;

      translate ([sz[0]-r, r, r])
         sphere (r = r) ;

      translate ([r, sz[1]-r, r])
         sphere (r = r) ;

      translate ([sz[0]-r, sz[1]-r, r])
         sphere (r = r) ;
   }
   
   module place_leads()
   {
      translate ([-p/2, 0, 0]) children() ;
      translate ([p/2, 0, 0]) children() ;
   }
   
   translate ([p/2, 0, 0])
   {
      color ([0.8, 0.5, 0.3])
      {
         translate ([-sz[0]/2, -sz[1]/2, offs])
            render()
               hull()
               {
                  horz() ;
                  translate ([0, 0, sz[2] - r])
                     horz() ;
               }
               
         place_leads()
            hull()
            {
               cylinder (d = d, h = delta) ;
               
               translate ([0, 0, offs + r])
                  cylinder (d = d + offs*1.2, h = delta) ;
            }
      }
      
      color ([0.8, 0.8, 0.8])
         place_leads()
            translate ([0, 0, -2])
               cylinder (d = d, h = 2 + offs) ;
   }
}

module sqp10w()
{
   sz = [48, 10, 9] ;
   D = 0.7 ;
   
   color ([0.9, 0.9, 0.7])
      translate ([-sz[0]/2, -sz[1]/2])
         cube (sz) ;
   
   module lead()
   {
      color ([0.8, 0.8, 0.8])
         translate ([-52/2, 0])
         {
            translate ([0, 0, -2.2])
               cylinder (d = D, h = 2.2 + sz[2] / 2 - 2) ;
            
            translate ([2, 0, sz[2] / 2 - 2])
               rotate ([90, 0, 180])
                  rotate_extrude (angle = 90)
                     translate ([2, 0, 0])
                        circle (d = D) ;
         }
   }
   
   lead() ;
   mirror([1, 0, 0])
      lead() ;
}

module g3mb()
{
   color ([0.2, 0.2, 0.2])
      translate ([-24.5/2, -1.4])
         cube ([24.5, 5.5, 20.5]) ;
      
   color ([0.85, 0.85, 0.85])
      for (n = [-3, -4, 1, 4])
         translate ([n * 2.54 - 0.35, -0.2, -3.9])
            cube ([0.7, 0.4, 3.9]) ;
}
