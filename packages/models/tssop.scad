module part_tssop (N = 8, wide = true)
{
   $fn = 30 ;
   delta = 1e-3 ;
   
   nums = [ 8, 10, 14, 16, 20, 24, 28, 44, 48, 56, 64, 80 ] ;
   widths = [ 3, 3, 4.4, 4.4, 4.4, 4.4, 4.4, 4.4, 6.1, 4.4, 6.1, 6.1 ] ;
   widths_w = [ 4.4, 3, 4.4, 4.4, 4.4, 4.4, 6.1, 4.4, 6.1, 6.1, 6.1, 6.1 ] ;
   pitches = [ 0.65, 0.5, 0.65, 0.65, 0.65, 0.65, 0.65, 0.5, 0.5, 0.4, 0.5, 0.4 ] ;
   pitches_w = [ 0.65, 0.5, 0.65, 0.65, 0.65, 0.65, 0.65, 0.5, 0.5, 0.5, 0.5, 0.4 ] ;
   
   idx = search (N, nums)[0] ;
   width = wide ? widths_w[idx] : widths[idx] ;
   pitch = wide ? pitches_w[idx] : pitches[idx] ;
   height = width == 3 ? 0.85 : 0.9 ;
   length = (N/2 - 1) * pitch + 1.15 ;
   lead_w = pitch * 0.4 ;
   
   module lead()
      color ([0.8, 0.8, 0.8])
         translate ([-lead_w/2, 0])
         {
            translate ([0, -1.05])
               cube ([lead_w, 0.75, 0.15]) ;
            
            translate ([0, -0.3-0.15/2, 0.15/2])
               cube ([lead_w, 0.15, 1.05/2]) ;

            translate ([0, -0.3, 1.05/2])
               cube ([lead_w, 0.5, 0.15]) ;
          
            translate ([0, -0.3, 0.15/2])
               rotate ([0, 90, 0])
                  cylinder (d = 0.15, h = lead_w) ;

            translate ([0, -0.3, 1.05/2+0.15/2])
               rotate ([0, 90, 0])
                  cylinder (d = 0.15, h = lead_w) ;
         }
         
   module leads()
      for (n = [0 : N/2 - 1])
         translate ([-pitch*(N/2 - 1)/2 + n*pitch, -width/2])
            lead() ;
   
   rotate ([0, 0, -90])
   {
      translate ([-length/2, -width/2, 0.15])
      {
         color ([0.2, 0.2, 0.2])
            hull()
            {
               translate ([0.1, 0.1, 0])
                  cube ([length - 0.2, width - 0.2, height]) ;
               
               translate ([0, 0, height/2])
                  cube ([length, width, delta]) ;
            }
            
         color ("gray")
            translate ([0.7, 0.7, height])
               cylinder (d = 0.5, h = delta) ;
      }     
     
      leads() ;
      
      mirror ([0, 1, 0])
         leads() ;
   }
}
