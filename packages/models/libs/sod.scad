$fn = 30 ;

module sod323()
{
   module lead()
   {
      color ([0.9, 0.9, 0.9])
      {
         translate ([0, -0.35/2, 0.4])
            rotate ([-90, 0, 0])
               rotate_extrude (angle = 90)
                  translate ([0.25, 0, 0])
                     square ([0.15, 0.35]) ;
         
         translate ([0.25, -0.35/2, 0.4])
            cube ([0.15, 0.35, 1.35/2 - 0.4]) ;

         translate ([0.5, 0.35/2, 1.35/2])
            rotate ([90, -90, 0])
               rotate_extrude (angle = 90)
                  translate ([0.1, 0, 0])
                     square ([0.15, 0.35]) ;
      }
   }

   translate ([-0.5-0.9, 0, 0])
      lead() ;
   
   mirror ([1, 0, 0])
      translate ([-0.5-0.9, 0, 0])
         lead() ;
   
   translate ([-0.9, -0.7, 0])
      color ([0.3, 0.3, 0.3])
         cube ([1.8, 1.4, 1.35]) ;
}


module sod123()
{
   A = 1.35 ;
   D = 1.8 ;
   E = 2.84 ;
   He = 3.86 ;
   
   c = 0.15 ;
   L = 0.25 ;
   b = 0.71 ;
   l2 = 0.15 ;
   
   module lead()
   {
      color ([0.9, 0.9, 0.9])
      {
         translate ([0, -b/2, L+c])
            rotate ([-90, 0, 0])
               rotate_extrude (angle = 90)
                  translate ([L, 0, 0])
                     square ([c, b]) ;
         
         translate ([L, -b/2, L+c])
            cube ([c, b, A/2 - (L+c)]) ;

         translate ([c+l2+L, b/2, A/2])
            rotate ([90, -90, 0])
               rotate_extrude (angle = 90)
                  translate ([l2, 0, 0])
                     square ([c, b]) ;
      }
   }

   translate ([-He/2, 0, 0])
      lead() ;
   
   translate ([He/2, 0, 0])
      mirror ([1, 0, 0])
         lead() ;
   
   translate ([He/2 - E/2 - He/2, -D/2, 0])
      color ([0.3, 0.3, 0.3])
         cube ([E, D, A]) ;

}
