
Element["" "" "" "" 21.0000mm 18.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[0.0000 0.0000 2.6000mm 20.00mil 2.7524mm 1.3000mm "" "1" "edge2"]
	Pin[539.00mil 0.0000 2.6000mm 20.00mil 2.7524mm 1.3000mm "" "2" "edge2"]
	Pin[525.00mil 465.00mil 2.6000mm 20.00mil 2.7524mm 1.3000mm "" "3" "edge2"]
	Pin[125.00mil 465.00mil 2.6000mm 20.00mil 2.7524mm 1.3000mm "" "4" "edge2"]
	ElementLine [-3.0094mm 13.8000mm 15.9906mm 13.8000mm 0.1000mm]
	ElementLine [15.9906mm 13.8000mm 15.9906mm -2.0000mm 0.1000mm]
	ElementLine [15.9906mm -2.0000mm -3.0094mm -2.0000mm 0.1000mm]
	ElementLine [-3.0094mm -2.0000mm -3.0094mm 13.8000mm 0.1000mm]

	)
