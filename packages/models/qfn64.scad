use <qfn-impl.scad>

module part_qfn64()
{
   qfn (S = 7, H = 0.9, N = 64, P = 0.4, W = 0.18, L = 0.3, PS = 5.2) ;
}
