
Element["" "" "" "" 58.0000mm 41.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-8.0000mm 0.0000 2.0000mm 30.00mil 2.1524mm 0.9000mm "" "1" "edge2"]
	Pin[7.0000mm 0.0000 2.0000mm 30.00mil 2.1524mm 0.9000mm "" "2" "edge2"]
	ElementLine [-9.5000mm -2.0000mm 8.5000mm -2.0000mm 10.00mil]
	ElementLine [8.5000mm -2.0000mm 8.5000mm -14.5000mm 10.00mil]
	ElementLine [8.5000mm -14.5000mm -9.5000mm -14.5000mm 10.00mil]
	ElementLine [-9.5000mm -14.5000mm -9.5000mm -2.0000mm 10.00mil]

	)
