
Element["" "" "" "" 650.00mil 1250.00mil 0.0000 0.0000 0 100 ""]
(
	Pin[0.0000 0.0000 160.00mil 40.00mil 166.00mil 60.00mil "" "1" "edge2"]
	Pin[0.0000 760.00mil 160.00mil 40.00mil 166.00mil 60.00mil "" "2" "edge2"]
	Pin[0.0000 1520.00mil 160.00mil 40.00mil 166.00mil 60.00mil "" "3" "edge2"]
	Pin[2850.00mil 0.0000 160.00mil 40.00mil 166.00mil 60.00mil "" "6" "edge2"]
	Pin[2850.00mil 760.00mil 160.00mil 40.00mil 166.00mil 60.00mil "" "5" "edge2"]
	Pin[2850.00mil 1520.00mil 160.00mil 40.00mil 166.00mil 60.00mil "" "4" "edge2"]
	ElementLine [-105.00mil -415.00mil 2955.00mil -415.00mil 10.00mil]
	ElementLine [2955.00mil -415.00mil 2955.00mil 1935.00mil 10.00mil]
	ElementLine [2955.00mil 1935.00mil -105.00mil 1935.00mil 10.00mil]
	ElementLine [-105.00mil 1935.00mil -105.00mil -415.00mil 10.00mil]
	ElementLine [-105.00mil 375.00mil 2955.00mil 375.00mil 10.00mil]
	ElementLine [-105.00mil 1145.00mil 2955.00mil 1145.00mil 10.00mil]
	ElementArc [325.00mil -5.00mil 120.00mil 120.00mil 270 90 10.00mil]
	ElementArc [325.00mil -5.00mil 120.00mil 120.00mil 180 90 10.00mil]
	ElementArc [325.00mil -5.00mil 120.00mil 120.00mil 90 90 10.00mil]
	ElementArc [325.00mil -5.00mil 120.00mil 120.00mil 0 90 10.00mil]
	ElementArc [325.00mil -5.00mil 70.00mil 70.00mil 270 90 10.00mil]
	ElementArc [325.00mil -5.00mil 70.00mil 70.00mil 180 90 10.00mil]
	ElementArc [325.00mil -5.00mil 70.00mil 70.00mil 90 90 10.00mil]
	ElementArc [325.00mil -5.00mil 70.00mil 70.00mil 0 90 10.00mil]
	ElementArc [325.00mil 765.00mil 120.00mil 120.00mil 270 90 10.00mil]
	ElementArc [325.00mil 765.00mil 120.00mil 120.00mil 180 90 10.00mil]
	ElementArc [325.00mil 765.00mil 120.00mil 120.00mil 90 90 10.00mil]
	ElementArc [325.00mil 765.00mil 120.00mil 120.00mil 0 90 10.00mil]
	ElementArc [325.00mil 765.00mil 70.00mil 70.00mil 270 90 10.00mil]
	ElementArc [325.00mil 765.00mil 70.00mil 70.00mil 180 90 10.00mil]
	ElementArc [325.00mil 765.00mil 70.00mil 70.00mil 90 90 10.00mil]
	ElementArc [325.00mil 765.00mil 70.00mil 70.00mil 0 90 10.00mil]
	ElementArc [325.00mil 1525.00mil 120.00mil 120.00mil 270 90 10.00mil]
	ElementArc [325.00mil 1525.00mil 120.00mil 120.00mil 180 90 10.00mil]
	ElementArc [325.00mil 1525.00mil 120.00mil 120.00mil 90 90 10.00mil]
	ElementArc [325.00mil 1525.00mil 120.00mil 120.00mil 0 90 10.00mil]
	ElementArc [325.00mil 1525.00mil 70.00mil 70.00mil 270 90 10.00mil]
	ElementArc [325.00mil 1525.00mil 70.00mil 70.00mil 180 90 10.00mil]
	ElementArc [325.00mil 1525.00mil 70.00mil 70.00mil 90 90 10.00mil]
	ElementArc [325.00mil 1525.00mil 70.00mil 70.00mil 0 90 10.00mil]
	ElementArc [2525.00mil -5.00mil 120.00mil 120.00mil 270 90 10.00mil]
	ElementArc [2525.00mil -5.00mil 120.00mil 120.00mil 180 90 10.00mil]
	ElementArc [2525.00mil -5.00mil 120.00mil 120.00mil 90 90 10.00mil]
	ElementArc [2525.00mil -5.00mil 120.00mil 120.00mil 0 90 10.00mil]
	ElementArc [2525.00mil -5.00mil 70.00mil 70.00mil 270 90 10.00mil]
	ElementArc [2525.00mil -5.00mil 70.00mil 70.00mil 180 90 10.00mil]
	ElementArc [2525.00mil -5.00mil 70.00mil 70.00mil 90 90 10.00mil]
	ElementArc [2525.00mil -5.00mil 70.00mil 70.00mil 0 90 10.00mil]
	ElementArc [2525.00mil 765.00mil 120.00mil 120.00mil 270 90 10.00mil]
	ElementArc [2525.00mil 765.00mil 120.00mil 120.00mil 180 90 10.00mil]
	ElementArc [2525.00mil 765.00mil 120.00mil 120.00mil 90 90 10.00mil]
	ElementArc [2525.00mil 765.00mil 120.00mil 120.00mil 0 90 10.00mil]
	ElementArc [2525.00mil 765.00mil 70.00mil 70.00mil 270 90 10.00mil]
	ElementArc [2525.00mil 765.00mil 70.00mil 70.00mil 180 90 10.00mil]
	ElementArc [2525.00mil 765.00mil 70.00mil 70.00mil 90 90 10.00mil]
	ElementArc [2525.00mil 765.00mil 70.00mil 70.00mil 0 90 10.00mil]
	ElementArc [2525.00mil 1525.00mil 120.00mil 120.00mil 270 90 10.00mil]
	ElementArc [2525.00mil 1525.00mil 120.00mil 120.00mil 180 90 10.00mil]
	ElementArc [2525.00mil 1525.00mil 120.00mil 120.00mil 90 90 10.00mil]
	ElementArc [2525.00mil 1525.00mil 120.00mil 120.00mil 0 90 10.00mil]
	ElementArc [2525.00mil 1525.00mil 70.00mil 70.00mil 270 90 10.00mil]
	ElementArc [2525.00mil 1525.00mil 70.00mil 70.00mil 180 90 10.00mil]
	ElementArc [2525.00mil 1525.00mil 70.00mil 70.00mil 90 90 10.00mil]
	ElementArc [2525.00mil 1525.00mil 70.00mil 70.00mil 0 90 10.00mil]

	)
