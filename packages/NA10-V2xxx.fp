
Element["" "" "" "" 50.0000mm 50.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[23.5000mm 0.0000 3.0000mm 30.00mil 3.1524mm 1.1000mm "" "6" "edge2"]
	Pin[23.5000mm 5.0000mm 3.0000mm 30.00mil 3.1524mm 1.1000mm "" "7" "edge2"]
	Pin[23.5000mm 10.5000mm 3.0000mm 30.00mil 3.1524mm 1.1000mm "" "8" "edge2"]
	Pin[23.5000mm -5.0000mm 3.0000mm 30.00mil 3.1524mm 1.1000mm "" "5" "edge2"]
	Pin[23.5000mm -10.0000mm 3.0000mm 30.00mil 3.1524mm 1.1000mm "" "4" "edge2"]
	Pin[-23.5000mm 0.0000 3.0000mm 30.00mil 3.1524mm 1.1000mm "" "2" "edge2"]
	Pin[-23.5000mm -17.5000mm 3.0000mm 30.00mil 3.1524mm 1.1000mm "" "1" "square,edge2"]
	Pin[-23.5000mm 17.5000mm 3.0000mm 30.00mil 3.1524mm 1.1000mm "" "3" "edge2"]
	ElementLine [-27.5000mm -22.5000mm 27.5000mm -22.5000mm 15.00mil]
	ElementLine [27.5000mm -22.5000mm 27.5000mm 20.0000mm 15.00mil]
	ElementLine [25.0000mm 22.5000mm -27.5000mm 22.5000mm 15.00mil]
	ElementLine [-27.5000mm 22.5000mm -27.5000mm -22.5000mm 15.00mil]
	ElementLine [25.0000mm 22.5000mm 27.5000mm 20.0000mm 15.00mil]

	)
