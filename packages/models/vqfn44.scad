use <qfn-impl.scad>
module part_vqfn44()
{
   qfn (S = 7, H = 0.9, N = 44, P = 0.5, W = 0.23, L = 0.64, PS = 5.2) ;
}

part_vqfn44() ;