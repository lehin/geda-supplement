include <pcb.scad>

module part_hdra_50_4_m (flip = false)
{
   mirror ([flip ? 1 : 0, 0, 0])
      rotate ([0, 0, -90])
         xsm127x2r(2) ;
}
