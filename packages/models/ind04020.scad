module ind04020_part()
{
   sz = [4.6, 4.1, 2] ;
   lead_th = 0.15 ;
   body_sz = sz - [lead_th*2, 0, lead_th] ;
   
   module lead()
   {
      color ([0.95, 0.95, 0.95])
         translate ([-sz[0]/2, -1.5/2])
         {
            cube ([1, 1.5, lead_th]) ;
            cube ([0.2, 1.5, 0.7]) ;
         }
   }
   
   lead() ;
   mirror ([1, 0, 0])
      lead() ;
   
   color ([0.85, 0.85, 0.85])
      translate ([-body_sz[0]/2, -body_sz[1]/2, lead_th])
         cube (body_sz) ;
}
