module part_pv32h()
{
   translate ([0, 0.38, 0])
   {
      color ([0.7, 0.7, 0.7])
         cylinder (d = 6.35, h = 4) ;

      color ("white")
         translate ([0, 0, 4])
            cylinder (d = 3.56, h = 0.57) ;
   }

   color ([0.95, 0.95, 0.95])
      translate ([0, 0, -2])
      {
         cylinder (d = 0.46, h = 2) ;
         
         rotate ([0, 0, 45])
            translate ([2.54, 0, 0])
               cylinder (d = 0.46, h = 2) ;

         rotate ([0, 0, -45])
            translate ([-2.54, 0, 0])
               cylinder (d = 0.46, h = 2) ;
      }
}
