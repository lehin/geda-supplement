
Element["" "" "" "" 40.4000mm 49.1000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-0.7500mm -1.8000mm -0.7500mm -1.3000mm 0.3000mm 0.4000mm 0.7000mm "" "8" "square"]
	Pad[-0.2500mm -1.8000mm -0.2500mm -1.3000mm 0.3000mm 0.4000mm 0.7000mm "" "7" "square"]
	Pad[0.2500mm -1.8000mm 0.2500mm -1.3000mm 0.3000mm 0.4000mm 0.7000mm "" "6" "square"]
	Pad[0.7500mm -1.8000mm 0.7500mm -1.3000mm 0.3000mm 0.4000mm 0.7000mm "" "5" "square"]
	Pad[-0.7500mm 1.3000mm -0.7500mm 1.8000mm 0.3000mm 0.4000mm 0.7000mm "" "1" "square,edge2"]
	Pad[-0.2500mm 1.3000mm -0.2500mm 1.8000mm 0.3000mm 0.4000mm 0.7000mm "" "2" "square,edge2"]
	Pad[0.2500mm 1.3000mm 0.2500mm 1.8000mm 0.3000mm 0.4000mm 0.7000mm "" "3" "square,edge2"]
	Pad[0.7500mm 1.3000mm 0.7500mm 1.8000mm 0.3000mm 0.4000mm 0.7000mm "" "4" "square,edge2"]
	ElementLine [-1.0500mm -1.2000mm 1.0500mm -1.2000mm 0.2000mm]
	ElementLine [1.0500mm -1.2000mm 1.0500mm 1.2000mm 0.2000mm]
	ElementLine [1.0500mm 1.2000mm -1.0500mm 1.2000mm 0.2000mm]
	ElementLine [-1.0500mm 1.2000mm -1.0500mm -1.2000mm 0.2000mm]
	ElementArc [-0.6500mm 0.8000mm 0.1000mm 0.1000mm 270 90 0.2000mm]
	ElementArc [-0.6500mm 0.8000mm 0.1000mm 0.1000mm 180 90 0.2000mm]
	ElementArc [-0.6500mm 0.8000mm 0.1000mm 0.1000mm 90 90 0.2000mm]
	ElementArc [-0.6500mm 0.8000mm 0.1000mm 0.1000mm 0 90 0.2000mm]

	)
