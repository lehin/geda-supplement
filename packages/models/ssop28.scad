use <packages.scad>

module part_ssop28()
{
   rotate ([0, 0, 90])
      SSOP (28) ;
}
