
Element["" "" "" "" 31.5000mm 31.5000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-11.1000mm -8.7000mm 1.5416mm 0.02mil 1.6940mm 1.4400mm "" "0" "edge2"]
	Pin[-11.1000mm 8.7000mm 1.5416mm 0.02mil 1.6940mm 1.4400mm "" "0" "edge2"]
	Pin[11.1000mm 8.7000mm 1.5416mm 0.02mil 1.6940mm 1.4400mm "" "0" "edge2"]
	Pin[11.1000mm -8.7000mm 1.5416mm 0.02mil 1.6940mm 1.4400mm "" "0" "edge2"]
	ElementLine [-11.5000mm -11.5000mm 11.5000mm -11.5000mm 15.00mil]
	ElementLine [11.5000mm -11.5000mm 11.5000mm 11.5000mm 15.00mil]
	ElementLine [11.5000mm 11.5000mm -11.5000mm 11.5000mm 15.00mil]
	ElementLine [-11.5000mm 11.5000mm -11.5000mm -11.5000mm 15.00mil]
	ElementArc [0.0000 0.0000 11.5000mm 11.5000mm 270 90 15.00mil]
	ElementArc [0.0000 0.0000 11.5000mm 11.5000mm 180 90 15.00mil]
	ElementArc [0.0000 0.0000 11.5000mm 11.5000mm 90 90 15.00mil]
	ElementArc [0.0000 0.0000 11.5000mm 11.5000mm 0 90 15.00mil]

	)
