module part_tbh_18650_2a_p()
{
   dims = [ 77.90, 40.11, 21.41 ] ;
   
   module place_axis()
      for (y = [-19.3/2, 19.3/2])
         translate ([0, y])
            children() ;

   module box()
   {
      translate ([-dims[0]/2, -dims[1]/2])
         difference()
         {
            cube (dims) ;
            
            translate ([2.3, 1.3, 2.5])
               cube (dims - [2.3*2, 1.3*2, 0]) ;

            translate ([2.3, -1, dims[2] - 5])
               cube (dims - [2.3*2, -2, 0]) ;
         }
         
      translate ([-dims[0]/2, -1.1/2])
         cube ([dims[0], 1.1, 12]) ;
   }

   color ([0.3, 0.3, 0.3])
      difference()
      {
         box() ;
         
         place_axis()
            for (x = [-55.6/2, 55.6/2])
               translate ([x, 0, -1])
                  cylinder (d = 3, h = 5) ;
      }
   
   color ([0.85, 0.85, 0.85])
      place_axis()
         for (x = [-72.37/2, 72.37/2])
            translate ([x - 0.4/2, -1.5/2, -3.55])
               cube ([0.4, 1.5, 3.55]) ;
         
   color ([0.7, 0.4, 0.7])
      place_axis()
         translate ([-65/2, 0, 18/2+3])
            rotate ([0, 90, 0])
               cylinder (d = 18, h = 65) ;
}