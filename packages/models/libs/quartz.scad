module clk_quartz (D = 3, L = 8, offs = 2)
{
   module impl()
   {
      color ("white")
         translate ([0, 2, offs + D/2])
         {
            rotate ([-90, 0, 0])
               cylinder (d = D, h = L) ;
         }

      color ([0.7, 0.7, 0.7])
      {
         translate ([-1.25/2, 0, offs + D/2])
         {
            rotate ([-90, 0, 0])
               cylinder (d = 0.4, h = 2) ;
            
            sphere (d = 0.4) ;
         }

         translate ([1.25/2, 0, offs + D/2])
         {
            rotate ([-90, 0, 0])
               cylinder (d = 0.4, h = 2) ;

            sphere (d = 0.4) ;
         }
         
         H = offs + D/2 ;
         A = asin (1.25 / H / 2) ;
         h = H - cos (A) * H ;
         
         translate ([1.25/2, 0, H])
            rotate ([0, -A])
               translate ([0, 0, -H])
                  cylinder (d = 0.4, h = H) ;

         translate ([-1.25/2, 0, H])
            rotate ([0, A])
               translate ([0, 0, -H])
                  cylinder (d = 0.4, h = H) ;
         
         translate ([-1.25, 0, h])
            sphere (d = 0.4) ;

         translate ([1.25, 0, h])
            sphere (d = 0.4) ;
         
         translate ([-1.25, 0, -2])
            cylinder (d = 0.4, h = 2 + h) ;

         translate ([1.25, 0, -2])
            cylinder (d = 0.4, h = 2 + h) ;
      }
   }
   
   translate ([1.25, 0, 0])
      rotate ([0, 0, 180])
         impl() ;
}