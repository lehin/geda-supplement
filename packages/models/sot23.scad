use <packages.scad>

module part_sot23()
{
   translate ([-0.75 - 0.55/2, 0.95, 0])
       rotate ([0, 0, 90])
           SOT23();
}
