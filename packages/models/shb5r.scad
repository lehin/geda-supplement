module part_shb5r()
{
   delta = 1e-3 ;
   
   rotate ([0, 0, 90])
   {
      translate ([-10.1/2, -12.5/2])
      {
         color ([0.95, 0.95, 0.85])
            cube ([10.1, 12.5, 7.9]) ;

         color ([0.2, 0.2, 0.2])
            translate ([0, 0, 7.9])
               cube ([10.1, 12.5, delta]) ;
      }
      
      color ([1,0.3,0.2])
         for (n = [-2:2])
            translate ([-5/2, -1.74/2 + n*2.54, 7.9])
               cube ([5, 1.74, 3*delta]) ;

      color ([0.85, 0.85, 0.85])
         for (m = [0,1])
            mirror ([m, 0, 0])
               for (n = [-2:2])
                  translate ([-7.4/2, n*2.54, -3.4])
                     cylinder (d = 0.51, h = 3.4) ;
   }
}
