
Element["" "" "" "" 55.5000mm 41.5000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-7.5000mm 0.0000 2.0000mm 30.00mil 2.1524mm 0.9000mm "" "1" "edge2"]
	Pin[7.5000mm 0.0000 2.0000mm 30.00mil 2.1524mm 0.9000mm "" "2" "edge2"]
	ElementLine [-9.0000mm -3.5000mm 9.0000mm -3.5000mm 10.00mil]
	ElementLine [9.0000mm -3.5000mm 9.0000mm 3.5000mm 10.00mil]
	ElementLine [9.0000mm 3.5000mm -9.0000mm 3.5000mm 10.00mil]
	ElementLine [-9.0000mm 3.5000mm -9.0000mm -3.5000mm 10.00mil]

	)
