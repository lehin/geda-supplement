use <qfn-impl.scad>

module part_qfn20 ()
{
   qfn (S = 4, H = 0.75, N = 20, P = 0.5, W = 0.23, L = 0.4, PS = 2.6) ;
}
