
Element["" "" "" "" 11.5000mm 11.5000mm 0.0000 0.0000 0 100 ""]
(
	Pad[1.2500mm -0.7500mm 1.4000mm -0.7500mm 0.3000mm 20.00mil 0.8080mm "" "12" "square,edge2"]
	Pad[1.2500mm -0.2500mm 1.4000mm -0.2500mm 0.3000mm 20.00mil 0.8080mm "" "11" "square,edge2"]
	Pad[1.2500mm 0.7500mm 1.4000mm 0.7500mm 0.3000mm 20.00mil 0.8080mm "" "9" "square,edge2"]
	Pad[1.2500mm 0.2500mm 1.4000mm 0.2500mm 0.3000mm 20.00mil 0.8080mm "" "10" "square,edge2"]
	Pad[0.7500mm 1.2000mm 0.7500mm 1.3500mm 0.3000mm 20.00mil 0.8080mm "" "8" "square,edge2"]
	Pad[0.2500mm 1.2000mm 0.2500mm 1.3500mm 0.3000mm 20.00mil 0.8080mm "" "7" "square,edge2"]
	Pad[-0.7500mm 1.2000mm -0.7500mm 1.3500mm 0.3000mm 20.00mil 0.8080mm "" "5" "square,edge2"]
	Pad[-0.2500mm 1.2000mm -0.2500mm 1.3500mm 0.3000mm 20.00mil 0.8080mm "" "6" "square,edge2"]
	Pad[-1.3500mm 0.7500mm -1.2000mm 0.7500mm 0.3000mm 20.00mil 0.8080mm "" "4" "square"]
	Pad[-1.3500mm 0.2500mm -1.2000mm 0.2500mm 0.3000mm 20.00mil 0.8080mm "" "3" "square"]
	Pad[-1.3500mm -0.7500mm -1.2000mm -0.7500mm 0.3000mm 20.00mil 0.8080mm "" "1" "square"]
	Pad[-1.3500mm -0.2500mm -1.2000mm -0.2500mm 0.3000mm 20.00mil 0.8080mm "" "2" "square"]
	Pad[-0.7500mm -1.3500mm -0.7500mm -1.2000mm 0.3000mm 20.00mil 0.8080mm "" "16" "square"]
	Pad[-0.2500mm -1.3500mm -0.2500mm -1.2000mm 0.3000mm 20.00mil 0.8080mm "" "15" "square"]
	Pad[0.7500mm -1.3500mm 0.7500mm -1.2000mm 0.3000mm 20.00mil 0.8080mm "" "13" "square"]
	Pad[0.2500mm -1.3500mm 0.2500mm -1.2000mm 0.3000mm 20.00mil 0.8080mm "" "14" "square"]
	ElementLine [-1.3500mm 1.3500mm -1.3500mm -1.3500mm 10.00mil]
	ElementLine [1.4000mm 1.3500mm -1.3500mm 1.3500mm 10.00mil]
	ElementLine [1.4000mm -1.3500mm 1.4000mm 1.3500mm 10.00mil]
	ElementLine [-1.3000mm -1.3500mm 1.4000mm -1.3500mm 10.00mil]
	ElementArc [-0.7500mm -0.7500mm 0.1000mm 0.1000mm 270 90 10.00mil]
	ElementArc [-0.7500mm -0.7500mm 0.1000mm 0.1000mm 0 90 10.00mil]
	ElementArc [-0.7500mm -0.7500mm 0.1000mm 0.1000mm 90 90 10.00mil]
	ElementArc [-0.7500mm -0.7500mm 0.1000mm 0.1000mm 180 90 10.00mil]

	)
