module capacitor_impl (L, W, H, P, Tw, Th)
{
   delta = 1e-3 ;

   module lead (P, Tw, Th)
   {
      color ([0.9, 0.9, 0.9])
         translate ([-delta, -Tw/2, -delta])
         {
            translate ([0.3/2, 0, 0.3/2])
               rotate ([-90, 0, 0])
                  cylinder (d = 0.3, h = Tw) ;
            
            translate ([0, 0, 0.3/2])
               cube ([0.3, Tw, Th - 0.3/2]) ;
            
            translate ([0.3/2, 0, 0])
               cube ([P - 0.3/2, Tw, 0.3]) ;
         }
   }

   color ([0.9, 0.8, 0.2])
   {
      hull()
      {
         translate ([0, 0, Th])
            cube ([L, W, delta], center = true) ;

         translate ([0, 0, H])
            cube ([L - 0.3*2, W - 0.2, delta], center = true) ;
      }
      
      hull()
      {
         cube ([L - 0.3*2, W - 0.2, delta], center = true) ;

         translate ([0, 0, Th])
            cube ([L, W, delta], center = true) ;
      }
   }
   
   translate ([-L/2, 0, 0])
      lead (P, Tw, Th) ;

   translate ([L/2, 0, 0])
      mirror ([1, 0, 0])
         lead (P, Tw, Th) ;
}

module tantalum (sz)
{
   sizes = "ABCDE" ;
   L = [ 3.2, 3.5, 6.0, 7.3, 7.3 ] ;
   W = [ 1.6, 2.8, 3.2, 4.3, 4.3 ] ;
   H = [ 1.6, 1.9, 2.5, 2.8, 4 ] ;
   P = [ 0.8, 0.8, 1.3, 1.3, 1.3 ] ;
   Tw = [ 1.2, 2.2, 2.2, 2.4, 2.4 ] ;
   
   idx = search (sz, sizes)[0] ;
   capacitor_impl (L[idx], W[idx], H[idx], P[idx], Tw[idx], H[idx]/2) ;
}
