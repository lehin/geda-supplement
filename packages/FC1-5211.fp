
Element["" "" "" "" 51.0000mm 50.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-36.5000mm 0.0000 3.0000mm 30.00mil 3.1524mm 1.6000mm "" "1" "square,edge2"]
	Pin[36.5000mm 0.0000 3.0000mm 30.00mil 3.1524mm 1.6000mm "" "2" "edge2"]
	ElementLine [-38.8500mm -10.4500mm 38.8500mm -10.4500mm 10.00mil]
	ElementLine [38.8500mm 10.4500mm 38.8500mm -10.4500mm 10.00mil]
	ElementLine [-38.8500mm 10.4500mm 38.8500mm 10.4500mm 10.00mil]
	ElementLine [-38.8500mm 10.4500mm -38.8500mm -10.4500mm 10.00mil]

	)
