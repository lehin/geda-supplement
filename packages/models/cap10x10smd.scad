include <pcb.scad>

module part_cap10x10smd()
{
   rotate ([0, 0, -90])
      cap10x10() ;
}
