
Element["" "" "" "" 10.0000mm 11.0000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-0.0500mm 0.0000 0.0500mm 0.0000 1.0000mm 30.00mil 1.7620mm "" "1" "square"]
	Pad[-0.0500mm 1.6000mm 0.0500mm 1.6000mm 1.0000mm 30.00mil 1.7620mm "" "2" "square"]
	Pad[4.6500mm 0.0000 4.7500mm 0.0000 1.0000mm 30.00mil 1.7620mm "" "6" "square,edge2"]
	Pad[4.6500mm 1.6000mm 4.7500mm 1.6000mm 1.0000mm 30.00mil 1.7620mm "" "5" "square,edge2"]
	Pad[-0.1500mm 3.1000mm 0.3500mm 3.1000mm 0.8000mm 20.00mil 1.3080mm "" "3" "square"]
	Pad[4.3500mm 3.1000mm 4.8500mm 3.1000mm 0.8000mm 20.00mil 1.3080mm "" "4" "square,edge2"]
	ElementLine [-0.2500mm 3.4000mm -0.2500mm -1.8000mm 0.1000mm]
	ElementLine [-0.2500mm -1.8000mm 4.9500mm -1.8000mm 0.1000mm]
	ElementLine [4.9500mm -1.8000mm 4.9500mm 3.4000mm 0.1000mm]
	ElementLine [4.9500mm 3.4000mm -0.2500mm 3.4000mm 0.1000mm]
	ElementLine [1.7500mm -1.8000mm -0.2500mm -0.5000mm 0.1000mm]
	ElementLine [4.9500mm -0.5000mm 2.9500mm -1.8000mm 0.1000mm]
	ElementArc [0.1500mm -1.4000mm 0.2000mm 0.2000mm 0 90 0.1000mm]
	ElementArc [0.1500mm -1.4000mm 0.2000mm 0.2000mm 90 90 0.1000mm]
	ElementArc [0.1500mm -1.4000mm 0.2000mm 0.2000mm 180 90 0.1000mm]
	ElementArc [0.1500mm -1.4000mm 0.2000mm 0.2000mm 270 90 0.1000mm]

	)
