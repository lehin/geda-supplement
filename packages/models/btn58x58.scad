module part_btn58x58()
{
   S = 5.8 ;
   translate ([-S/2, -S/2])
   {
      color ([0.2, 0.2, 0.2])
         cube ([S,S,4.3]) ;
      
      color ([0.6, 0.6, 0.6])
         translate ([0, 0, 4.3])
            cube ([S,S,1]) ;
   }

   color ([0.9, 0.9, 0.9])
   {
      translate ([-3.5/2, -3.5/2])
         cube ([3.5,3.5,7.7]) ;

      translate ([-2.45/2, -2/2])
         cube ([2.45,2,10.1]) ;
   }
   
   color ([0.85, 0.85, 0.85])
      for (m = [0,1])
         mirror ([0, m, 0])
            for (x = [-2:2:2])
               translate ([x - 0.6/2, -4.5/2 - 0.3/2, -3.5])
                  cube ([0.6, 0.3, 3.5]) ;
}
