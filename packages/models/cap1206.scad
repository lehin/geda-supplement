use <chip-c.scad>

module part_cap0603()
{
   chip_c (L=3.2,T=0.5,W=1.8,H=1.8) ;
}
