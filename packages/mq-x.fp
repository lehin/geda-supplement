
Element["" "" "" "" 10.0000mm 10.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[3.3580mm -3.3577mm 2.4000mm 20.00mil 2.5524mm 1.2000mm "" "3" ""]
	Pin[3.3565mm 3.3583mm 2.4000mm 20.00mil 2.5524mm 1.2000mm "" "3" ""]
	Pin[-3.3575mm 3.3557mm 2.4000mm 20.00mil 2.5524mm 1.2000mm "" "4" ""]
	Pin[-3.3572mm -3.3580mm 2.4000mm 20.00mil 2.5524mm 1.2000mm "" "4" ""]
	Pin[4.7500mm 0.0000 2.4000mm 20.00mil 2.5524mm 1.2000mm "" "1" ""]
	Pin[-4.7500mm 0.0000 2.4000mm 20.00mil 2.5524mm 1.2000mm "" "2" ""]
	ElementArc [0.0000 0.0000 8.4000mm 8.4000mm 0 90 5.00mil]
	ElementArc [0.0000 0.0000 8.4000mm 8.4000mm 90 90 5.00mil]
	ElementArc [0.0000 0.0000 8.4000mm 8.4000mm 180 90 5.00mil]
	ElementArc [0.0000 0.0000 8.4000mm 8.4000mm 270 90 5.00mil]
	ElementArc [0.0000 0.0000 9.5000mm 9.5000mm 0 90 10.00mil]
	ElementArc [0.0000 0.0000 9.5000mm 9.5000mm 90 90 10.00mil]
	ElementArc [0.0000 0.0000 9.5000mm 9.5000mm 180 90 10.00mil]
	ElementArc [0.0000 0.0000 9.5000mm 9.5000mm 270 90 10.00mil]

	)
