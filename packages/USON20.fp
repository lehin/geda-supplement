
Element["" "" "" "" 27.8000mm 37.8000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-1.8000mm -1.3000mm -1.8000mm -0.5000mm 0.2000mm 0.4000mm 0.6000mm "" "20" "square"]
	Pad[-1.4000mm -1.3000mm -1.4000mm -0.5000mm 0.2000mm 0.4000mm 0.6000mm "" "19" "square"]
	Pad[-1.0000mm -1.3000mm -1.0000mm -0.5000mm 0.2000mm 0.4000mm 0.6000mm "" "18" "square"]
	Pad[-0.6000mm -1.3000mm -0.6000mm -0.5000mm 0.2000mm 0.4000mm 0.6000mm "" "17" "square"]
	Pad[-0.2000mm -1.3000mm -0.2000mm -0.5000mm 0.2000mm 0.4000mm 0.6000mm "" "16" "square"]
	Pad[0.2000mm -1.3000mm 0.2000mm -0.5000mm 0.2000mm 0.4000mm 0.6000mm "" "15" "square"]
	Pad[0.6000mm -1.3000mm 0.6000mm -0.5000mm 0.2000mm 0.4000mm 0.6000mm "" "14" "square"]
	Pad[1.0000mm -1.3000mm 1.0000mm -0.5000mm 0.2000mm 0.4000mm 0.6000mm "" "13" "square"]
	Pad[1.4000mm -1.3000mm 1.4000mm -0.5000mm 0.2000mm 0.4000mm 0.6000mm "" "12" "square"]
	Pad[1.8000mm -1.3000mm 1.8000mm -0.5000mm 0.2000mm 0.4000mm 0.6000mm "" "11" "square"]
	Pad[-1.8000mm 0.4000mm -1.8000mm 1.3000mm 0.2000mm 0.4000mm 0.6000mm "" "1" "square,edge2"]
	Pad[-1.4000mm 0.5000mm -1.4000mm 1.3000mm 0.2000mm 0.4000mm 0.6000mm "" "2" "square,edge2"]
	Pad[-1.0000mm 0.5000mm -1.0000mm 1.3000mm 0.2000mm 0.4000mm 0.6000mm "" "3" "square,edge2"]
	Pad[-0.6000mm 0.5000mm -0.6000mm 1.3000mm 0.2000mm 0.4000mm 0.6000mm "" "4" "square,edge2"]
	Pad[-0.2000mm 0.5000mm -0.2000mm 1.3000mm 0.2000mm 0.4000mm 0.6000mm "" "5" "square,edge2"]
	Pad[0.2000mm 0.5000mm 0.2000mm 1.3000mm 0.2000mm 0.4000mm 0.6000mm "" "6" "square,edge2"]
	Pad[0.6000mm 0.5000mm 0.6000mm 1.3000mm 0.2000mm 0.4000mm 0.6000mm "" "7" "square,edge2"]
	Pad[1.0000mm 0.5000mm 1.0000mm 1.3000mm 0.2000mm 0.4000mm 0.6000mm "" "8" "square,edge2"]
	Pad[1.4000mm 0.5000mm 1.4000mm 1.3000mm 0.2000mm 0.4000mm 0.6000mm "" "9" "square,edge2"]
	Pad[1.8000mm 0.5000mm 1.8000mm 1.3000mm 0.2000mm 0.4000mm 0.6000mm "" "10" "square,edge2"]
	ElementLine [-2.0000mm -1.1000mm 2.0000mm -1.1000mm 0.1500mm]
	ElementLine [2.0000mm -1.1000mm 2.0000mm 0.9000mm 0.1500mm]
	ElementLine [2.0000mm 0.9000mm -2.0000mm 0.9000mm 0.1500mm]
	ElementLine [-2.0000mm 0.9000mm -2.0000mm -1.1000mm 0.1500mm]

	)
