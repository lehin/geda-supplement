use <chip-c.scad>

module part_cap0603()
{
   chip_c (L=1.6, T=0.35, W=0.8,H=0.8);
}
