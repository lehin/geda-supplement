
Element["" "" "" "" 125.00mil 150.00mil 0.0000 0.0000 0 100 ""]
(
	Pad[-50.00mil -110.00mil -50.00mil -55.00mil 25.00mil 20.00mil 45.00mil "" "1" "square"]
	Pad[0.0000 -110.00mil 0.0000 -55.00mil 25.00mil 20.00mil 45.00mil "" "3" "square"]
	Pad[50.00mil -110.00mil 50.00mil -55.00mil 25.00mil 20.00mil 45.00mil "" "5" "square"]
	Pad[-50.00mil 50.00mil -50.00mil 105.00mil 25.00mil 20.00mil 45.00mil "" "2" "square,edge2"]
	Pad[0.0000 50.00mil 0.0000 105.00mil 25.00mil 20.00mil 45.00mil "" "4" "square,edge2"]
	Pad[50.00mil 50.00mil 50.00mil 105.00mil 25.00mil 20.00mil 45.00mil "" "6" "square,edge2"]
	ElementLine [-75.00mil -130.00mil 75.00mil -130.00mil 7.00mil]
	ElementLine [-75.00mil 125.00mil 75.00mil 125.00mil 7.00mil]
	ElementLine [-75.00mil -130.00mil -75.00mil 125.00mil 7.00mil]
	ElementLine [75.00mil -130.00mil 75.00mil 125.00mil 7.00mil]

	)
