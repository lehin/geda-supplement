
Element["" "" "" "" 7.5000mm 35.5000mm 0.0000 0.0000 0 100 ""]
(
	Pad[0.0000 -4.7500mm 0.0000 -3.2500mm 2.5000mm 0.8000mm 3.3000mm "" "1" "square"]
	Pad[0.0000 3.2500mm 0.0000 4.7500mm 2.5000mm 0.8000mm 3.3000mm "" "2" "square,edge2"]
	ElementLine [5.1500mm -4.1500mm 4.1500mm -5.1500mm 10.00mil]
	ElementLine [-5.1500mm 5.1500mm -5.1500mm -4.2000mm 10.00mil]
	ElementLine [-5.1500mm -4.2000mm -4.2000mm -5.1500mm 10.00mil]
	ElementLine [-5.1500mm -4.1500mm -5.1500mm -4.2000mm 10.00mil]
	ElementLine [5.1500mm 5.1500mm -5.1500mm 5.1500mm 10.00mil]
	ElementLine [5.1500mm -4.1500mm 5.1500mm 5.1500mm 10.00mil]
	ElementLine [-4.2000mm -5.1500mm 4.1500mm -5.1500mm 10.00mil]

	)
