
Element["" "" "" "" 61.7000mm 50.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-1.7000mm 0.0000 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "1" "edge2"]
	Pin[1.7000mm 0.0000 1.4000mm 20.00mil 1.5524mm 0.6000mm "" "2" "edge2"]
	ElementLine [-1.3000mm -2.1000mm 1.3000mm -2.1000mm 10.00mil]
	ElementLine [-1.3000mm 2.1000mm 1.3000mm 2.1000mm 10.00mil]
	ElementArc [0.0000 0.0000 2.5000mm 2.5000mm 270 90 10.00mil]
	ElementArc [0.0000 0.0000 2.5000mm 2.5000mm 180 90 10.00mil]
	ElementArc [0.0000 0.0000 2.5000mm 2.5000mm 90 90 10.00mil]
	ElementArc [0.0000 0.0000 2.5000mm 2.5000mm 0 90 10.00mil]

	)
