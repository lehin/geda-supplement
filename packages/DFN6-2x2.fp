
Element["" "" "" "" 50.0000mm 50.0000mm 0.0000 0.0000 0 100 ""]
(
	Pad[0.9500mm 0.0000 1.0500mm 0.0000 0.6000mm 20.00mil 1.1080mm "" "5" "square,edge2"]
	Pad[-1.0500mm 0.0000 -0.9500mm 0.0000 0.6000mm 20.00mil 1.1080mm "" "2" "square"]
	Pad[0.8500mm -0.9500mm 1.1500mm -0.9500mm 0.4000mm 20.00mil 0.9080mm "" "6" "square,edge2"]
	Pad[-1.1500mm -0.9500mm -0.8500mm -0.9500mm 0.4000mm 20.00mil 0.9080mm "" "1" "square"]
	Pad[0.8500mm 0.9500mm 1.1500mm 0.9500mm 0.4000mm 20.00mil 0.9080mm "" "4" "square,edge2"]
	Pad[-1.1500mm 0.9500mm -0.8500mm 0.9500mm 0.4000mm 20.00mil 0.9080mm "" "3" "square"]
	ElementLine [-1.0000mm 1.0000mm 1.0000mm 1.0000mm 0.1000mm]
	ElementLine [1.0000mm 1.0000mm 1.0000mm -1.0000mm 0.1000mm]
	ElementLine [1.0000mm -1.0000mm -0.7500mm -1.0000mm 0.1000mm]
	ElementLine [-1.0000mm 1.0000mm -1.0000mm -0.7500mm 0.1000mm]
	ElementLine [-1.0000mm -0.7500mm -0.7500mm -1.0000mm 0.1000mm]

	)
