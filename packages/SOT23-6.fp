
Element["" "" "U0" "" 92.0mil 64.0mil 0.0 0.0 0 100 ""]
(
	Pad[29.0mil 950.352um 67.0mil 950.352um 20.0mil 12.0mil 26.0mil "4" "4" "square"]
	Pad[29.0mil 392.0nm 67.0mil 392.0nm 20.0mil 12.0mil 26.0mil "5" "5" "square"]
	Pad[29.0mil -949.568um 67.0mil -949.568um 20.0mil 12.0mil 26.0mil "6" "6" "square"]
	Pad[-1.701408mm -949.568um -736.208um -949.568um 20.0mil 12.0mil 26.0mil "1" "1" "square"]
	Pad[-1.701408mm 392.0nm -736.208um 392.0nm 20.0mil 12.0mil 26.0mil "2" "2" "square"]
	Pad[-1.701408mm 950.352um -736.208um 950.352um 20.0mil 12.0mil 26.0mil "3" "3" "square"]
	ElementLine [2.210192mm 1.498992mm -2.209408mm 1.498992mm 10.0mil]
	ElementLine [-2.209408mm 1.498992mm -2.209408mm -1.498208mm 10.0mil]
	ElementLine [-2.209408mm -1.498208mm 2.210192mm -1.498208mm 10.0mil]
	ElementLine [2.210192mm -1.498208mm 2.210192mm 1.498992mm 10.0mil]

)
