
Element["" "" "" "" 39.0000mm 47.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[0.0000 0.0000 2.5000mm 30.00mil 2.6524mm 0.9000mm "" "1" "square,edge2"]
	Pin[0.0000 5.0000mm 2.5000mm 30.00mil 2.6524mm 0.9000mm "" "2" "edge2"]
	Pin[29.4000mm 10.2000mm 2.5000mm 30.00mil 2.6524mm 0.9000mm "" "4" "edge2"]
	Pin[29.4000mm -5.2000mm 2.5000mm 30.00mil 2.6524mm 0.9000mm "" "3" "edge2"]
	ElementLine [-2.3000mm 12.6000mm -2.3000mm -7.6000mm 10.00mil]
	ElementLine [31.7000mm 12.6000mm -2.3000mm 12.6000mm 10.00mil]
	ElementLine [31.7000mm -7.6000mm 31.7000mm 12.6000mm 10.00mil]
	ElementLine [-2.3000mm -7.6000mm 31.7000mm -7.6000mm 10.00mil]

	)
