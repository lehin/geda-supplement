use <packages.scad>

module part_so16w()
{
   rotate ([0, 0, 90])
       SOIC (16, wide = true) ;
}
