use <pcb.scad>

module part_cap10x16r()
{
   translate ([5/2, 0, 0])
      rotate ([0, 0, 180])
         cap10x16r() ;
}
