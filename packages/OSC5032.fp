
Element["" "" "" "" 10.0000mm 10.0000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-2.0000mm -0.2000mm -2.0000mm 0.2000mm 2.0000mm 30.00mil 2.7620mm "" "1" "square"]
	Pad[2.0000mm -0.2000mm 2.0000mm 0.2000mm 2.0000mm 30.00mil 2.7620mm "" "2" "square"]
	ElementLine [-2.5000mm 1.6000mm -2.5000mm 1.4000mm 10.00mil]
	ElementLine [2.5000mm 1.6000mm 2.5000mm 1.4000mm 10.00mil]
	ElementLine [-2.5000mm 1.6000mm 2.5000mm 1.6000mm 10.00mil]
	ElementLine [-2.5000mm -1.6000mm -2.5000mm -1.4000mm 10.00mil]
	ElementLine [2.5000mm -1.6000mm 2.5000mm -1.4000mm 10.00mil]
	ElementLine [-2.5000mm -1.6000mm 2.5000mm -1.6000mm 10.00mil]

	)
