
Element["" "" "" "" 78.0000mm 115.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-2.3500mm -2.8000mm 1.2000mm 0.5000mm 1.3524mm 0.5000mm "" "5" "edge2"]
	Pin[-3.3500mm -2.8000mm 1.2000mm 0.5000mm 1.3524mm 0.5000mm "" "5" "edge2"]
	Pin[2.3500mm -2.8000mm 1.2000mm 0.5000mm 1.3524mm 0.5000mm "" "5" "edge2"]
	Pin[3.3500mm -2.8000mm 1.2000mm 0.5000mm 1.3524mm 0.5000mm "" "5" "edge2"]
	Pin[-4.1000mm -0.5000mm 1.2000mm 0.5000mm 1.3524mm 0.5000mm "" "5" "edge2"]
	Pin[-4.1000mm 0.5000mm 1.2000mm 0.5000mm 1.3524mm 0.5000mm "" "5" "edge2"]
	Pin[4.1000mm -0.5000mm 1.2000mm 0.5000mm 1.3524mm 0.5000mm "" "5" "edge2"]
	Pin[4.1000mm 0.5000mm 1.2000mm 0.5000mm 1.3524mm 0.5000mm "" "5" "edge2"]
	Pad[2.3500mm -2.8000mm 3.3500mm -2.8000mm 1.2000mm 30.00mil 1.9620mm "" "5" "edge2"]
	Pad[-3.3500mm -2.8000mm -2.3500mm -2.8000mm 1.2000mm 30.00mil 1.9620mm "" "5" ""]
	Pad[0.0000 -124.02mil 0.0000 -86.61mil 15.75mil 20.00mil 35.75mil "" "3" "square"]
	Pad[25.59mil -124.02mil 25.59mil -86.61mil 15.75mil 20.00mil 35.75mil "" "4" "square"]
	Pad[51.18mil -124.02mil 51.18mil -86.61mil 15.75mil 20.00mil 35.75mil "" "5" "square"]
	Pad[-25.59mil -124.02mil -25.59mil -86.61mil 15.75mil 20.00mil 35.75mil "" "2" "square"]
	Pad[-51.18mil -124.02mil -51.18mil -86.61mil 15.75mil 20.00mil 35.75mil "" "1" "square"]
	Pad[47.24mil 0.0000 49.21mil 0.0000 74.80mil 20.00mil 94.80mil "" "5" "square,edge2"]
	Pad[-51.18mil 0.0000 -47.24mil 0.0000 74.80mil 20.00mil 94.80mil "" "5" "square"]
	Pad[-4.1000mm -0.5000mm -4.1000mm 0.5000mm 1.2000mm 30.00mil 1.9620mm "" "5" "edge2"]
	Pad[4.1000mm -0.5000mm 4.1000mm 0.5000mm 1.2000mm 30.00mil 1.9620mm "" "5" "edge2"]
	ElementLine [-147.64mil -116.14mil -147.64mil 57.09mil 0.39mil]
	ElementLine [147.64mil 57.09mil 147.64mil -116.14mil 0.39mil]
	ElementLine [147.64mil -116.14mil -147.64mil -116.14mil 0.39mil]
	ElementLine [-147.64mil 57.09mil 147.64mil 57.09mil 5.39mil]

	)
