
Element["" "" "U0" "" 625.0mil 2.525in 0.0 0.0 0 100 ""]
(
	Pad[95.0mil 75.0mil 115.0mil 75.0mil 20.0mil 20.0mil 708.0um "" "5" "square"]
	Pad[95.0mil 25.0mil 115.0mil 25.0mil 20.0mil 20.0mil 708.0um "" "6" "square"]
	Pad[95.0mil -25.0mil 115.0mil -25.0mil 20.0mil 20.0mil 708.0um "" "7" "square"]
	Pad[95.0mil -75.0mil 115.0mil -75.0mil 20.0mil 20.0mil 708.0um "" "8" "square"]
	Pad[-115.0mil 75.0mil -95.0mil 75.0mil 20.0mil 20.0mil 708.0um "" "4" "square"]
	Pad[-115.0mil 25.0mil -95.0mil 25.0mil 20.0mil 20.0mil 708.0um "" "3" "square"]
	Pad[-115.0mil -25.0mil -95.0mil -25.0mil 20.0mil 20.0mil 708.0um "" "2" "square"]
	Pad[-115.0mil -75.0mil -95.0mil -75.0mil 20.0mil 20.0mil 708.0um "" "1" "square"]
	Pad[0.0 -450.0um 0.0 450.0um 2.6mm 20.0mil 2.8mm "" "9" "square"]
	ElementLine [-135.0mil -100.0mil -135.0mil 100.0mil 7.0mil]
	ElementLine [-135.0mil 100.0mil 135.0mil 100.0mil 7.0mil]
	ElementLine [135.0mil 100.0mil 135.0mil -100.0mil 7.0mil]
	ElementLine [135.0mil -100.0mil 25.0mil -100.0mil 7.0mil]
	ElementLine [-25.0mil -100.0mil -135.0mil -100.0mil 7.0mil]
	ElementArc [0.0 -100.0mil 25.0mil 25.0mil 0.000000 90.000000 7.0mil]
	ElementArc [0.0 -100.0mil 25.0mil 25.0mil 90.000000 90.000000 7.0mil]

)
