module part_cr2032_smd()
{
   color ([0.95, 0.95, 0.75])
      difference()
      {
         delta = 1e-2 ;
         
         union()
         {
            translate ([-22/2, -16/2])
               cube ([22, 16, 5.5]) ;
               
            translate ([-28.5/2, -7/2])
               cube ([28.5, 7, 5.5]) ;
         }
         
         translate ([0, 0, 1.5])
            cylinder (d = 20, h = 5.5) ;
         
         translate ([-26.5/2, -5/2, 1.5])
            cube ([26.5, 5, 5.5] ) ;
         
         translate ([-28.5/2-delta, -3.5/2, -delta])
            cube ([1 + 2*delta, 3.5, 5.5 + 2*delta]) ;

         translate ([28.5/2-1-delta, -3.5/2, -delta])
            cube ([1 + 2*delta, 3.5, 5.5 + 2*delta]) ;

         translate ([-22/2, -16/2, -delta])
            rotate ([0, 0, 45])
               translate ([-2/2, -4/2])
                  cube ([2, 4, 5.5 + 2*delta]) ;
      }

   color ([0.9,0.9,0.9])
   {
      translate ([-32.10/2, -3.5/2])
      {
         cube ([2.8, 3.5, 0.45]) ;
         
         translate ([2.8-0.45, -1.5/2])
            cube ([0.45, 5, 4.5]) ;

         translate ([2.8-0.45, -1.5/2, 4.5-0.45])
            cube ([3, 5, 0.45]) ;

         translate ([2.8+2.7-0.45, -1.5/2, 1.5])
            cube ([0.45, 5, 3]) ;
         
         translate ([2.8+2.7-0.45, 0, 1.5])
            cube ([1 + 0.45, 3.5, 0.45]) ;

         translate ([2.8+2.7+1-0.45, 0, 1.5])
            cube ([0.45, 3.5, 3.5+0.45]) ;

         translate ([2.8+2.7+1-0.45, 0, 5])
         {
            cube ([1.5, 1, 0.45]) ;
            
            translate ([0, 3.5 - 1])
               cube ([1.5, 1, 0.45]) ;
         }
      }

      translate ([32.10/2, -3.5/2])
         mirror ([1, 0, 0])
         {
            cube ([2.8, 3.5, 0.45]) ;
            
            translate ([2.8-0.45, -1.5/2])
               cube ([0.45, 5, 4.5]) ;

            translate ([2.8-0.45, -1.5/2, 4.5-0.45])
               cube ([3, 5, 0.45]) ;

            translate ([2.8+3-0.45, -1.5/2, 1.5])
               cube ([0.45, 5, 3]) ;
            
            translate ([2.8+3-0.45, 0, 1.5])
               rotate ([0, -10, 0])
                  cube ([7, 3.5, 0.45]) ;
         }
   }

   color ([1,1,1])
      translate ([0, 0, 1.7])
         cylinder (d=20, h = 3.2) ;
}
