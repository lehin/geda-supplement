
Element["" "" "" "" 60.5500mm 60.2500mm 0.0000 0.0000 0 100 ""]
(
	Pad[-0.4500mm 0.0000 0.4500mm 0.0000 0.4000mm 20.00mil 0.9080mm "" "1" "square"]
	Pad[1.1500mm 0.0000 2.0500mm 0.0000 0.4000mm 20.00mil 0.9080mm "" "6" "square,edge2"]
	Pad[-0.4500mm 0.6500mm 0.4500mm 0.6500mm 0.4000mm 20.00mil 0.9080mm "" "2" "square"]
	Pad[1.1500mm 0.6500mm 2.0500mm 0.6500mm 0.4000mm 20.00mil 0.9080mm "" "5" "square,edge2"]
	Pad[-0.4500mm 1.3000mm 0.4500mm 1.3000mm 0.4000mm 20.00mil 0.9080mm "" "3" "square"]
	Pad[1.1500mm 1.3000mm 2.0500mm 1.3000mm 0.4000mm 20.00mil 0.9080mm "" "4" "square,edge2"]
	ElementLine [-0.2000mm -0.3500mm 1.8000mm -0.3500mm 0.1500mm]
	ElementLine [1.8000mm -0.3500mm 1.8000mm 1.6500mm 0.1500mm]
	ElementLine [1.8000mm 1.6500mm -0.2000mm 1.6500mm 0.1500mm]
	ElementLine [-0.2000mm 1.6500mm -0.2000mm -0.3500mm 0.1500mm]
	ElementArc [0.1000mm -0.0500mm 0.1000mm 0.1000mm 0 90 0.1500mm]
	ElementArc [0.1000mm -0.0500mm 0.1000mm 0.1000mm 270 90 0.1500mm]
	ElementArc [0.1000mm -0.0500mm 0.1000mm 0.1000mm 180 90 0.1500mm]
	ElementArc [0.1000mm -0.0500mm 0.1000mm 0.1000mm 90 90 0.1500mm]

	)
