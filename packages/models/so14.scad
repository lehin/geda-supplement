use <packages.scad>

module part_so14()
{
   rotate ([0, 0, 90])
       SOIC (14) ;
}
