module do214_impl (L, W, H, P, Tw, Th)
{
   delta = 1e-3 ;

   module lead (P, Tw, Th)
   {
      color ([0.9, 0.9, 0.9])
         translate ([-delta, -Tw/2, -delta])
         {
            translate ([0.3/2, 0, 0.3/2])
               rotate ([-90, 0, 0])
                  cylinder (d = 0.3, h = Tw) ;

            translate ([0.3/2, 0, Th])
               rotate ([-90, 0, 0])
                  cylinder (d = 0.3, h = Tw) ;
            
            translate ([0, 0, 0.3/2])
               cube ([0.3, Tw, Th - 0.3/2]) ;
            
            translate ([0.3/2, 0, 0])
               cube ([3*P - 0.3/2, Tw, 0.3]) ;

            translate ([0.3/2, 0, Th - 0.3/2])
               cube ([2*P - 0.3/2, Tw, 0.3]) ;
         }
   }

   color ([0.3, 0.3, 0.3])
   {
      hull()
      {
         translate ([0, 0, Th])
            cube ([L, W, delta], center = true) ;

         translate ([0, 0, H])
            cube ([L*0.95, W*0.95, delta], center = true) ;
      }
      
      hull()
      {
         cube ([L*0.95, W*0.95, delta], center = true) ;

         translate ([0, 0, Th])
            cube ([L, W, delta], center = true) ;
      }
   }
   
   translate ([-L/2-P, 0, 0])
      lead (P, Tw, Th) ;

   translate ([L/2+P, 0, 0])
      mirror ([1, 0, 0])
         lead (P, Tw, Th) ;
}

module do214 (idx)
{
   L = [ 4.57, 7.11, 4.6 ] ;
   W = [ 3.94, 6.22, 2.90 ] ;
   H = [ 2.5, 2.62, 2.45 ] ;
   Tw = [ 2.21, 3.2, 1.65 ] ;
   Tl = [ 5.59, 8.13, 5.35 ] ;
   
   do214_impl (L[idx], W[idx], H[idx], (Tl[idx]-L[idx]) / 2, Tw[idx], H[idx]/2) ;
}
