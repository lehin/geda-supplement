
Element["" "" "" "" 38.0000mm 53.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[137.80mil 0.0000 90.00mil 20.00mil 96.00mil 30.00mil "" "2" "edge2"]
	Pin[0.0000 0.0000 90.00mil 20.00mil 96.00mil 30.00mil "" "1" "square,edge2"]
	Pin[275.59mil 0.0000 90.00mil 20.00mil 96.00mil 30.00mil "" "3" "edge2"]
	ElementLine [-68.90mil -98.43mil 8.7500mm -133.86mil 3.94mil]
	ElementLine [-68.90mil -98.43mil 8.7500mm -98.43mil 3.94mil]
	ElementLine [-68.90mil 133.86mil 8.7500mm 133.86mil 3.94mil]
	ElementLine [-68.90mil -133.86mil -68.90mil 133.86mil 3.94mil]
	ElementLine [8.7500mm 133.86mil 8.7500mm -133.86mil 3.94mil]
	ElementLine [8.7500mm -133.86mil -68.90mil -133.86mil 3.94mil]

	)
