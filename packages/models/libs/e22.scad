$fn = 30 ;
delta = 1e-3 ;

pcb_sz = [24, 38.5, 0.8] ;

module U_FL()
{
   color ([0.7, 0.7, 0.7])
      translate ([-1.3, -1.3, 0])
         cube ([2.6, 2.6, 0.35]) ;
   
   color ([0.9, 0.9, 0.9])
      difference()
      {
         cylinder (d = 2, h = 1.25) ;
         cylinder (d = 1.6, h = 1.25 + delta) ;
      }

   color ([0.9, 0.9, 0.5])
      cylinder (d = 0.6, h = 1.25) ;
}

module pad (simplify = false)
{
   color ([1, 0.9, 0.3])
      difference()
      {
         union()
         {
            translate ([-delta, -1.5/2, -delta])
               cube ([0.5, 1.5, pcb_sz[2] + 2*delta]) ;

            if (!simplify)
               translate ([-delta, -1.5/2, -delta])
                  cube ([1.25, 1.5, delta]) ;
         }

         if (!simplify)
            translate ([0, 0, -2*delta])
               cylinder (d = 0.8, h = pcb_sz[2] + 4*delta) ;
      }
}

module place_pads()
{
   module row()
   {
      for (n = [0 : 10])
         translate ([0, 5.46 + n * 2.54 + (n > 2 ? 7.6 - 2.54 : 0), 0])
            children() ;
   }
   
   row() children() ;
   translate ([pcb_sz[0], 0, 0]) mirror ([1, 0, 0]) row() children() ;
}

module E22400M30S()
{
   translate ([-pcb_sz[0]/2, -pcb_sz[1]/2])
   {
      difference()
      {
         color ([0, 0.4, 0])
            cube (pcb_sz) ;
         
         place_pads()
            pad (true) ;
      }

      place_pads()
         pad() ;
      
      color ([0.9, 0.9, 0.9])
         render()
            difference()
            {
               translate ([1, 0.5, pcb_sz[2]])
                  cube ([pcb_sz[0] - 2, pcb_sz[1] - 1, 3.87 - pcb_sz[2]]) ;
               
               cube ([6.3, 6.3, 3.87 + 2*delta]) ;
            }
            
      translate ([6.3/2, 6.3/2, pcb_sz[2]])
         U_FL() ;
   }
}

