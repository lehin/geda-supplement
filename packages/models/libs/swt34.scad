$fn = 30 ;
delta = 1e-3 ;

module swt34 (H)
{
   module fix()
      cylinder (d = 1.2, h = 0.5) ;
      
   module lead()
   {
      color ([0.8, 0.8, 0.8])
         translate ([-1, -1/2])
         {
            cube ([0.6, 1, 0.2]) ;
            translate ([0.4, 0])
            {
               cube ([0.2, 1, 1]) ;
               
               translate ([0, 0, 0.8])
                  cube ([0.6, 1, 0.2]) ;
            }
         }
   }
      
   color ([0.3, 0.3, 0.3])
      cylinder (d = 3.5, h = H) ;
      
   translate ([-3, -3])
   {
      color ([0.3, 0.3, 0.3])
      {
         cube ([6, 6, 3.3]) ;

         translate ([1, 1, 3.5]) fix() ;
         translate ([5, 1, 3.5]) fix() ;
         translate ([1, 5, 3.5]) fix() ;
         translate ([5, 5, 3.5]) fix() ;
      }

      color ([0.9, 0.9, 0.9])
         translate ([0, 0, 3.3])
            cube ([6, 6, 0.2]) ;
   }

   translate ([-3, -4.5/2]) lead() ;
   translate ([-3, 4.5/2]) lead() ;
   translate ([3, -4.5/2]) mirror ([1, 0, 0]) lead() ;
   translate ([3, 4.5/2]) mirror ([1, 0, 0]) lead() ;
}

swt34 (7) ;