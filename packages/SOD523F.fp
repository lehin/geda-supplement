
Element["" "" "" "" 50.0000mm 50.0000mm 0.0000 0.0000 0 100 ""]
(
	Pad[0.0000 -1.1500mm 0.0000 -0.8500mm 0.5000mm 20.00mil 1.0080mm "" "2" "square"]
	Pad[0.0000 0.8500mm 0.0000 1.1500mm 0.5000mm 20.00mil 1.0080mm "" "1" "square,edge2"]
	ElementLine [-0.4500mm -0.7500mm 0.4500mm -0.7500mm 10.00mil]
	ElementLine [0.4500mm -0.7500mm 0.4500mm 0.7500mm 10.00mil]
	ElementLine [0.4500mm 0.7500mm -0.4500mm 0.7500mm 10.00mil]
	ElementLine [-0.4500mm 0.7500mm -0.4500mm -0.7500mm 10.00mil]
	ElementLine [-0.4500mm -0.3000mm 0.4500mm -0.3000mm 10.00mil]

	)
