
Element["" "" "" "" 50.0000mm 50.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-6.2500mm -2.5000mm 1.8500mm 20.00mil 2.0024mm 1.1500mm "" "1" "edge2"]
	Pin[-6.2500mm 2.5000mm 1.8500mm 20.00mil 2.0024mm 1.1500mm "" "2" "edge2"]
	Pin[6.2500mm 2.5000mm 1.8500mm 20.00mil 2.0024mm 1.1500mm "" "2" "edge2"]
	Pin[6.2500mm -2.5000mm 1.8500mm 20.00mil 2.0024mm 1.1500mm "" "1" "edge2"]
	ElementLine [-6.0000mm -6.0000mm 6.0000mm -6.0000mm 10.00mil]
	ElementLine [6.0000mm -6.0000mm 6.0000mm 6.0000mm 10.00mil]
	ElementLine [6.0000mm 6.0000mm -6.0000mm 6.0000mm 10.00mil]
	ElementLine [-6.0000mm 6.0000mm -6.0000mm -6.0000mm 10.00mil]

	)
