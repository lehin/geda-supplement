use <pcb.scad>

module part_cap16x25r()
{
   translate ([7.5/2, 0, 0])
      rotate ([0, 0, 180])
         cap16x25r() ;
}