module part_sh10 (n)
{
   translate ([0, -1.415])
   {
      translate ([-n/2-1+0.05, 0])
         color ([0.9, 0.9, 0.7])
            cube ([n + 2 - 0.1, 2.9, 4.25]) ;
      
      color ([0.95, 0.95, 0.95])
      {
         for (i = [0 : n-1])
            translate ([i - (n-1)/2 - 0.15, -0.7])
               cube ([0.3, 0.7, 0.3]) ;
         
         for (x = [-n/2-1, n/2+1 - 0.3])
            translate ([x, 2.9 - 1.5])
               cube ([0.3, 1.5, 1.5]) ;
      }
   }
}
