module part_usba_1j()
{
   module impl()
   {
      color ([0.75, 0.75, 0.75])
      {
         translate ([-12.5/2, -1.3, 0])
         {
            ye = 7.3-5.75 ;
            
            translate ([0, 0, ye])
            {
               h0 = 5.75 ;
               cube ([12.5, 14.3, h0]) ;
               
               h1 = 7.7 ;
               y1 = (h1 - h0)/2 ;
               
               translate ([0, 14.1, -y1])
                  cube ([12.5, 0.2, h1]) ;
            }
            
            cube ([12.5, 8, ye]) ;
         }

         for (v = [0, 1])
            mirror ([v, 0, 0])
               translate ([-12.5/2, 13.0 - 10.28 - 1, -3.72])
                  cube ([0.3, 2, 3.72]) ;
      }

      color ([0.85, 0.85, 0.85])
         for (v = [0, 1])
            mirror ([v, 0, 0])
               for (x = [1, 3.5])
                  translate ([x, 0, -2.8])
                     cylinder (d = 0.7, h = 2.8) ;
   }
   
   rotate ([0, 0, 180])
      translate ([0, -6.07])
         impl() ;
}
