// qfn parametric model
//
// S - size of case
// H - height of case
// N - number of pins
// P - pins pitch
// W - pin width
// L - pin length
// PS - pad size

module qfn (S = 7, H = 0.9, N = 44, P = 0.5, W = 0.23, L = 0.64, PS = 5.2)
{
   $fn = 30 ;
   delta = 1e-3 ;
   leads = (N/4-1)*P ;
   leads_offs = (S - leads)/2 ;
   
   translate ([-S/2, -S/2])
      color ([0.3,0.3,0.3])
         cube ([S, S, H]) ;
   
   color ([0.8, 0.8, 0.8])
   {
      if (PS > 0)
         translate ([-PS/2, -PS/2, -0.02])
            cube ([PS, PS, 0.02]) ;

      for (j = [0 : 3])
         rotate ([0, 0, j*90])
            translate ([-leads/2 - W/2, -S/2-delta, -0.02])
               for (i = [0 : N/4-1])
                  translate ([P * i, 0, 0])
                  {
                     cube ([W, L, 0.02]) ;
                     cube ([W, 0.02, 0.22]) ;
                  }
   }
   
   color ("gray")
      translate ([-leads/2, leads/2, H])
         cylinder (d = 0.5, h = delta) ;
}
