
Element["" "" "" "" 60.0000mm 60.0000mm 0.0000 0.0000 0 100 ""]
(
	Pad[0.0000 -7.1500mm 0.0000 -6.6500mm 3.5000mm 30.00mil 4.2620mm "" "1" "square"]
	Pad[0.0000 6.6500mm 0.0000 7.1500mm 3.5000mm 30.00mil 4.2620mm "" "2" "square,edge2"]
	ElementLine [-8.0500mm -8.0500mm -8.0500mm 8.0500mm 10.00mil]
	ElementLine [8.0500mm 8.0500mm 8.0500mm -8.0500mm 10.00mil]
	ElementLine [-8.0500mm -8.0500mm 8.0500mm -8.0500mm 10.00mil]
	ElementLine [-8.0500mm 8.0500mm 8.0500mm 8.0500mm 10.00mil]

	)
