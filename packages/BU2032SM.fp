
Element["" "" "" "" 58.0000mm 61.0000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-14.6500mm -0.5000mm -14.6500mm 0.5000mm 3.2000mm 40.00mil 4.2160mm "" "1" "square"]
	Pad[14.6500mm -0.5000mm 14.6500mm 0.5000mm 3.2000mm 40.00mil 4.2160mm "" "2" "square"]
	ElementLine [-14.5500mm -2.5000mm -14.5500mm -3.5000mm 10.00mil]
	ElementLine [-14.5500mm 2.5000mm -14.5500mm 3.5000mm 10.00mil]
	ElementLine [-14.5500mm -3.5000mm -11.0000mm -3.5000mm 10.00mil]
	ElementLine [-14.5500mm 3.5000mm -11.0000mm 3.5000mm 10.00mil]
	ElementLine [14.5500mm -2.5000mm 14.5500mm -3.5000mm 10.00mil]
	ElementLine [14.5500mm -3.5000mm 11.0000mm -3.5000mm 10.00mil]
	ElementLine [14.7500mm 2.5000mm 14.7500mm 3.5000mm 10.00mil]
	ElementLine [14.7500mm 3.5000mm 11.0000mm 3.5000mm 10.00mil]
	ElementLine [-11.0000mm -8.0000mm -11.0000mm 6.0000mm 10.00mil]
	ElementLine [-9.0000mm 8.0000mm 11.0000mm 8.0000mm 10.00mil]
	ElementLine [11.0000mm 8.0000mm 11.0000mm -8.0000mm 10.00mil]
	ElementLine [11.0000mm -8.0000mm -11.0000mm -8.0000mm 10.00mil]
	ElementLine [-11.0000mm 6.0000mm -9.0000mm 8.0000mm 10.00mil]

	)
