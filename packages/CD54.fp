
Element["" "" "" "" 21.6500mm 31.9000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-1.6500mm -1.9000mm 1.7000mm -1.9000mm 2.1500mm 30.00mil 2.9120mm "" "1" "square"]
	Pad[-1.6500mm 1.9500mm 1.7000mm 1.9500mm 2.1500mm 30.00mil 2.9120mm "" "2" "square"]
	ElementArc [0.1500mm 0.1500mm 3.0000mm 3.0000mm 270 90 10.00mil]
	ElementArc [0.1500mm -0.0500mm 2.8000mm 2.8000mm 180 90 10.00mil]
	ElementArc [-0.0500mm -0.0500mm 3.0000mm 3.0000mm 90 90 10.00mil]
	ElementArc [-0.0500mm 0.1500mm 2.8000mm 2.8000mm 0 90 10.00mil]

	)
