use <qfn-impl.scad>

module part_vqfn20 ()
{
   qfn (S = 3, H = 0.85, N = 20, P = 0.4, W = 0.2, L = 0.4, PS = 1.7) ;
}
