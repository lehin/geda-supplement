
Element["" "" "" "" 8.0000mm 9.5000mm 0.0000 0.0000 0 100 ""]
(
	Pad[-2.2500mm 0.0000 -2.0500mm 0.0000 2.3000mm 30.00mil 3.0620mm "" "1" "square"]
	Pad[2.0500mm 0.0000 2.2500mm 0.0000 2.3000mm 30.00mil 3.0620mm "" "2" "square,edge2"]
	ElementLine [-2.2500mm -1.9500mm 2.2500mm -1.9500mm 10.00mil]
	ElementLine [2.2500mm -1.9500mm 2.2500mm 1.9500mm 10.00mil]
	ElementLine [2.2500mm 1.9500mm -2.2500mm 1.9500mm 10.00mil]
	ElementLine [-2.2500mm 1.9500mm -2.2500mm -1.9500mm 10.00mil]
	ElementLine [-1.2500mm -1.9500mm -1.2500mm 1.9500mm 10.00mil]

	)
