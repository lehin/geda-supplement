$fn = 30 ;

module evqwka()
{
   color ([0.2, 0.2, 0.2])
   {
      translate ([4.3, 0, -1])
      {
         cylinder (d = 1, h = 1) ;
         translate ([-9.5, 0])
            cylinder (d = 1, h = 1) ;
      }
      
      translate ([0, 9.6, -1.6])
         translate ([-1/2, -0.2/2])
            cube ([1, 0.2, 1.6]) ;
      
      translate ([0, 5.5, 3.65 - 1.95])
         cylinder (d = 15, h = 1.6) ;

      translate ([0, 5.5, 0])
         cylinder (d = 10, h = 1.55) ;
      
      translate ([-11.8+5.2, -4.7])
         cube ([11.8, 4.7 + 5.5, 1.55]) ;
   }
}
