
Element["" "" "" "" 52.0000mm 49.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[0.0000 0.0000 6.1000mm 20.00mil 6.2524mm 4.00mil "" "N" "edge2"]
	Pad[-6.7000mm 1.1000mm -6.0000mm 1.1000mm 1.6000mm 20.00mil 2.1080mm "" "1" "square"]
	Pad[6.0000mm -1.1000mm 6.7000mm -1.1000mm 1.6000mm 20.00mil 2.1080mm "" "2" "square,edge2"]
	ElementArc [0.0000 0.0000 4.0000mm 4.0000mm 270 90 10.00mil]
	ElementArc [0.0000 0.0000 4.0000mm 4.0000mm 180 90 10.00mil]
	ElementArc [0.0000 0.0000 4.0000mm 4.0000mm 90 90 10.00mil]
	ElementArc [0.0000 0.0000 4.0000mm 4.0000mm 0 90 10.00mil]

	)
