
Element["" "" "" "" 71.5000mm 47.0000mm 0.0000 0.0000 0 100 ""]
(
	Pin[-26.0000mm 0.0000 2.0000mm 30.00mil 2.1524mm 0.9000mm "" "1" "edge2"]
	Pin[26.0000mm 0.0000 2.0000mm 30.00mil 2.1524mm 0.9000mm "" "2" "edge2"]
	ElementLine [-24.0000mm -5.0000mm 24.0000mm -5.0000mm 10.00mil]
	ElementLine [24.0000mm -5.0000mm 24.0000mm 5.0000mm 10.00mil]
	ElementLine [24.0000mm 5.0000mm -24.0000mm 5.0000mm 10.00mil]
	ElementLine [-24.0000mm 5.0000mm -24.0000mm -5.0000mm 10.00mil]

	)
