
Element["" "" "" "" 11.3mm 10.2mm 0.0 0.0 0 100 ""]
(
	Pad[-2.4mm 0.0 -1.6mm 0.0 1.7mm 30.0mil 2.462mm "" "1" "square"]
	Pad[1.6mm 0.0 2.4mm 0.0 1.7mm 30.0mil 2.462mm "" "2" "square"]
	ElementLine [-3.172mm -1.45mm 3.559mm -1.45mm 10.0mil]
	ElementLine [3.559mm 1.45mm -3.172mm 1.45mm 10.0mil]
	ElementLine [-3.68mm -929.0um -3.68mm 976.0um 10.0mil]
	ElementLine [3.559mm 1.45mm 3.559mm -1.45mm 10.0mil]
	ElementLine [-3.68mm -929.0um -3.68mm -942.0um 10.0mil]
	ElementLine [-3.68mm -942.0um -3.172mm -1.45mm 10.0mil]
	ElementLine [-3.172mm 1.45mm -3.206mm 1.45mm 10.0mil]
	ElementLine [-3.206mm 1.45mm -3.68mm 976.0um 10.0mil]

)
