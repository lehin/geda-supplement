delta = 1e-3 ;

module rcube (dim = [1,1,1], rdim = -1, rdims = [0.1, 0.1, 0.1, 0.1])
{
   module impl (rdims)
   {
      hull()
      {
         if (rdims[0] > 0)
            translate ([rdims[0], rdims[0], 0]) cylinder (r=rdims[0], h=dim[2]) ;
         else
            cube ([delta, delta, dim[2]]) ;

         translate ([dim[0], 0, 0])
            if (rdims[1] > 0)
               translate ([-rdims[1], rdims[1], 0]) cylinder (r=rdims[1], h=dim[2]) ;
            else
               translate ([-delta, 0, 0])
                  cube ([delta, delta, dim[2]]) ;

         translate ([dim[0], dim[1], 0])
            if (rdims[2] > 0)
               translate ([-rdims[2], -rdims[2], 0]) cylinder (r=rdims[2], h=dim[2]) ;
            else
               translate ([-delta, -delta, 0])
                  cube ([delta, delta, dim[2]]) ;

         translate ([0, dim[1], 0])
            if (rdims[3] > 0)
               translate ([rdims[3], -rdims[3], 0]) cylinder (r=rdims[3], h=dim[2]) ;
            else
               translate ([0, -delta, 0])
                  cube ([delta, delta, dim[2]]) ;
      } ;
   }
   
   if (rdim >= 0)
      impl ([rdim, rdim, rdim, rdim]) ;
   else
      impl (rdims) ;
}
