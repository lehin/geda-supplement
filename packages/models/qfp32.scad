use <qfp.scad>

module part_qfp32()
{
   qfp(
      N1=8,
      N2=8,
      L1=9,
      L2=9,
      P=0.8,
      W=0.22,
      H=1.6,
      T=0.6,
      A=7,
      B=7,
      K=0.05
   );
}

