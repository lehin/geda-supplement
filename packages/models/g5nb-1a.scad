module part_g5nb_1a()
{
   color ([0.3, 0.3, 0.3])
      translate ([-1.05, -1.15])
         cube ([20.5, 7.2, 15.3]) ;
   
   color ([0.85, 0.85, 0.85])
      translate ([0, 0, -3.4])
         for (pt = [ [0,0], [0,4.7], [11.5,0], [18.5,0] ])
            translate (pt - [0.2, 0.4])
               cube ([0.4, 0.8, 3.4]) ;
}
