module part_btn5dir()
{
   module raw()
   {
      color ([0.5, 0.5, 0.5])
         translate ([-5, -5, 0])
            cube ([10, 10, 3.15]) ;
      
      color ([0.3, 0.3, 0.3])
      {
         cylinder (d = 6.5, h = 5.8) ;
         
         translate ([-1.6, -1.6])
            cube ([3.2, 3.2, 10]) ;
      }
      
      color ([0.85, 0.85, 0.85])
         for (n = [0,1])
            mirror ([n, 0, 0])
               translate ([-5 - 0.2, 0, -3.5])
               {
                  translate ([0, -0.8, 0])
                     cube ([0.2, 0.7, 4.5]) ;
                  
                  translate ([0, -6.5/2 - 0.45, 0])
                     cube ([0.2, 0.9, 4.5]) ;

                  translate ([0, 6.5/2 - 0.45, 0])
                     cube ([0.2, 0.9, 4.5]) ;
               }
   }

   translate ([0, 0, 0.3])
      rotate ([0, 0, -90])
         raw() ;
}
