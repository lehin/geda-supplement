module part_rgy20()
{
   rotate ([0, 0, 90])
   {
      delta = 1e-3 ;
      
      translate ([-4.65/2, -3.65/2])
         color ([0.3,0.3,0.3])
            cube ([4.65, 3.65, 0.9]) ;
      
      color ([0.8, 0.8, 0.8])
      {
         translate ([-3.15/2, -2.15/2, -0.05])
            cube ([3.15, 2.15, 0.05]) ;

         for (j = [0 : 1])
            rotate ([0, 0, j*180])
            {
               translate ([-3.5/2 - 0.3/2, -3.65/2-delta, -0.05])
                  for (i = [0:7])
                     translate ([0.5 * i, 0, 0])
                     {
                        cube ([0.3, 0.5, 0.05]) ;
                        cube ([0.3, 0.05, 0.25]) ;
                     }

               translate ([-4.65/2-delta, -1.5/2 - 0.3/2, -0.05])
                  for (i = [0:1])
                     translate ([0, 1.5 * i, 0])
                     {
                        cube ([0.5, 0.3, 0.05]) ;
                        cube ([0.05, 0.3, 0.25]) ;
                     }
            }
      }
      
      color ("gray")
         translate ([-3.5/2, -2.5/2, 0.9])
            cylinder (d = 0.3, h = delta) ;
   }
}
