delta = 1e-3 ;

SZ_tft = [ 60.26, 42.72, 2.35 ] ;
SZ_disp = [56.26, 40.32, delta] ;
SZ_va = [48.96, 36.72, 2*delta] ;

module display()
{
   color ([0.95, 0.95, 0.85])
      difference()
      {
         cube (SZ_tft) ;
         
         translate ([1.33 + SZ_disp[0] - delta, 4.75, 1])
            cube ([SZ_tft[0] - 1.33 - SZ_disp[0] + 2*delta, SZ_tft[1] - 2*4.75, SZ_tft[2] - 1 + delta]) ;
      }

   translate ([1.33, 1.2, SZ_tft[2]])
      color ([0.3, 0.3, 0.3])
         cube (SZ_disp) ;
         
   translate ([3.03, 3.00, SZ_tft[2]])
      color ([0.5, 0.5, 0.5])
         cube (SZ_va) ;

   translate ([3.03 + SZ_va[0] + 1.7, 1.2, 1])
      color ("black")
         cube ([SZ_disp[0] - SZ_va[0] - 2*1.7, SZ_disp[1], SZ_tft[2] - 1 + 2*delta]) ;
}


module cable()
{
   coords =
   [
      [0, 19.7, -19.7/2],
      [5, 36.79, -SZ_tft[1]/2 + 3.62],
      [13.66 + 5, 15.60, -6.14 - 1],
      [17.71 + 5, 15.60, -6.14 - 1]
   ] ;
   
   color ([0.8, 0.55, 0])
      for (i = [0 : len (coords)-2])
         translate ([coords[i][0], coords[i][2]])
            cube ([coords[i+1][0] - coords[i][0], coords[i][1], 0.25]) ;

   for (i = [0 : 17])
      translate ([17.71 + 5 - 2.5, -6.14 - 0.2 + 0.8 * i, -delta])
         color ([0.9, 0.8, 0.2])
            cube ([2.5, 0.4, 0.25 + 2*delta]) ;
}

module warp (X, R, cnt = 20, ext = [100, 100, 100])
{
   intersection()
   {
      children() ;
      translate ([0, -ext[1]/2, -delta])
         cube ([X, ext[1], ext[2] + 2*delta]) ;
   }
   
   tail = X + R*PI ;

   translate ([X, 0, -2*R])
      rotate ([0, 180, 0])
         intersection()
         {
            translate ([-tail, 0])
               children() ;
            
            translate ([0, -ext[1]/2, -delta])
               cube ([ext[0] - tail, ext[1], ext[2] + 2*delta]) ;
         }
      
   A = PI / cnt ;
   S = R * A ;
         
   for (i = [0 : cnt])
   {
      translate ([X, 0, -R])
         rotate ([0, i*A/PI*180, 0])
            translate ([0, 0, R])
               intersection()
               {
                  translate ([-X-S*i, 0])
                     children() ;

                  translate ([0, -ext[1]/2, -delta])
                     cube ([S, ext[1], ext[2] + 2*delta]) ;
               }
   }
}

module tft24_18 (OFFS = 0.3, Xoffs = 0)
{
   // На маленьких расстояниях де-факто сдвига не происходит - 
   //    панель ложится ровно так, как она показана на футпринте
   RW = 1 / 2 ;
//   RW = (1 + OFFS - 0.25) / 2 ;

   translate ([-60.26+19.16-2.5/2-RW*PI + Xoffs, 6.14, OFFS])
   {
      translate ([0, -SZ_tft[1]/2])
         display() ;

      translate ([SZ_tft[0] - (5 - 1.45), 0, 1])
         warp (5 - 1.45, RW, cnt = 50, ext = [50, 25, 1])
            cable() ;
   }
}

tft24_18() ;