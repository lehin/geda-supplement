use <qfn.scad>

module part_rak349mod()
{
   module impl()
   {
      module pad (simplified = false)
      {
         delta=1e-3 ;
         
         difference()
         {
            translate ([-0.8/2, -delta, -delta])
               cube ([0.8, 0.5, 1 + 2*delta]) ;
            
            if (!simplified)
               translate ([0, -0.1, -2*delta])
                  cylinder (d = 0.6, h = 1 + 4*delta) ;
         }
      }

      module place_pads()
      {
         for (i = [0:7])
            translate ([1 + i*2, 0])
               children() ;

         translate ([3, 14])
            mirror ([0, 1, 0])
               children() ;
         
         for (i = [0:5])
            translate ([0, 2 + i*2])
            {
               rotate ([0, 0, -90])
                  children() ;

               translate ([16, 0])
                  rotate ([0, 0, 90])
                     children() ;
            }
      }

      color ([0, 0.5, 0.3])
         difference()
         {
            cube ([16, 14, 1]) ;
            place_pads()
               pad (true) ;
         }

      $fn = 30 ;

      color ([1, 0.9, 0])
         place_pads()
            pad() ;
         
      translate ([2 + 3.5, 2 + 3.5, 1])
         qfn (S = 7, H = 0.9, N = 64, P = 0.4, W = 0.18, L = 0.3, PS = 5.2) ;
         
      translate ([1.8, 10, 1])
         color ([0.9, 0.9, 0.7])
            cube ([2.5, 2, 0.7]) ;
   }
   
   translate ([0, -12, 0])
      impl() ;
}
