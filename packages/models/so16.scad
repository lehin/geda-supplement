use <packages.scad>

module part_so16()
{
   rotate ([0, 0, 90])
       SOIC (16) ;
}
