module part_htu21d()
{
   color ([0.8, 0.85, 0.85])
   {
      translate ([-1.5/2, -2.4/2])
         linear_extrude (0.1)
            polygon ([ [ 0.5, 0 ], [ 1.5, 0 ], [1.5, 2.4], [0, 2.4], [0, 0.5] ]) ;
            
      for (x = [-1.3, 1.3])
         for (y = [-1 : 1])
            translate ([x - 0.2, y - 0.2])
               cube ([0.4, 0.4, 0.1]) ;
   }

   color ([0.3, 0.3, 0.3])
      translate ([-1.5, -1.5, 1e-2])
         cube ([3, 3, 0.9]) ;
}