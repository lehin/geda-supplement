use <sod.scad>

module part_sod323_2()
{
   rotate ([0, 0, 180])
      translate ([0.9 + 0.5 - 0.225, 0, 0])
         sod323() ;
}
