use <pcb.scad>

module part_cap8x11_5r()
{
   translate ([3.5/2, 0, 0])
      cap8x11r() ;
}
